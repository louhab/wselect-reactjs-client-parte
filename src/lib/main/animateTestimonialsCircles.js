import $ from "jquery";

export default function animateTestimonialsCircles(element) {
	if ($(element).hasClass("pxp-animate-bounce")) {
		setTimeout(function () {
			$(element).addClass("animate__animated animate__bounceIn");
		}, 200);
	}
}

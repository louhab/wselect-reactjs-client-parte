import React, { useId } from "react";
import { Controller } from "react-hook-form";

export const Input = ({ title, id, type, formik }) => {
	const placeholder = `Ajoutez votre ${title}`;
	return (
		<div className="mb-3">
			<label htmlFor={id} className="form-label">
				{title}
			</label>
			<input
				id={id}
				className="form-control"
				placeholder={placeholder}
				type={type || "text"}
				value={formik?.values[id]}
				onChange={formik.handleChange}
				onBlur={formik.handleBlur}
				name={id}
			/>
			{formik.touched[id] && formik.errors[id] ? (
				<p
					className="inputfieldlabel text-green-400"
					style={{ left: "23.03px", top: "100px", color: "red" }}
				>
					{formik.errors[id]}
				</p>
			) : (
				""
			)}
		</div>
	);
};

export function Select({ register, options, name, label, errors, ...rest }) {
	console.log('optixxxons' ,options)
	return (
		<div className="mb-3">
			<label htmlFor={name} className="form-label">
				{label}
			</label>
			<div className="input-group">
				<select
					{...register(name)}
					key={`pxp-key-select-${name}`}
					className="form-select"
					{...rest}
					style={{
						border:
							errors[name] && errors[name].message
								? "1px solid rgba(255, 0, 0, 0.3)"
								: null,
					}}
				>
					<option disabled value={""}>
						Select {label}
					</option>
					{options?.map((el, index) => {
						return (
							<option key={`${name}${index}`} value={el.companyName === ''? el?.id :el.companyName}>
								{el.companyName === ''? el?.name :el.companyName}
							</option>
						);
					})}
				</select>
			</div>
			<p
				style={{
					color: "#DC143C",
					fontSize: "13px",
					fontWeight: 200,
					fontStyle: "italic",
				}}
			>
				{errors[name] && errors[name].message}
			</p>
		</div>
	);
}

export const XSelect = ({ title, id, list, formik }) => {
	const temp = `pxp-key-select-${id}`;
	return (
		<div className="mb-3">
			<label htmlFor={id} className="form-label">
				{title}
			</label>
			<div className="input-group">
				<select
					key={temp}
					className="form-select"
					value={formik?.values[id] || "DEFAULT"}
					onChange={formik.handleChange}
					onBlur={formik.handleBlur}
					name={id}
				>
					<option disabled value={"DEFAULT"}>
						Select {title}
					</option>
					{list?.map((elem, index) => {
						return (
							<option key={`${id}${index}`} value={elem?.id}>
								{elem?.name}
							</option>
						);
					})}
				</select>
			</div>
		</div>
	);
};

export const Textarea = ({ name, label, register, errors, ...rest }) => {
	return (
		<div className="mb-3">
			<label htmlFor={name} className="form-label">
				{label}
			</label>
			<textarea
				id={name}
				className="form-control"
				placeholder={`Ajoutez votre ${label}`}
				{...rest}
				{...register(name)}
				style={{
					border:
						errors[name] && errors[name].message
							? "1px solid rgba(255, 0, 0, 0.3)"
							: null,
				}}
			/>
			<p
				style={{
					color: "#DC143C",
					fontSize: "13px",
					fontWeight: 200,
					fontStyle: "italic",
				}}
			>
				{errors[name] && errors[name].message}
			</p>
		</div>
	);
};

export const TextInput = ({
	name,
	label,
	register,
	type,
	className,
	alt,
	errors,
	...rest
}) => {
	return (
		<div className="mb-3">
			<label htmlFor={name} className={`form-label ${className}`}>
				{label}
			</label>
			<input
				id={name}
				className="form-control"
				type={type || "text"}
				placeholder={`Ajoutez votre ${alt || label}`}
				{...rest}
				{...register(name)}
				style={{
					border:
						errors[name] && errors[name].message
							? "1px solid rgba(255, 0, 0, 0.3)"
							: null,
				}}
			/>
			<p
				style={{
					color: "#DC143C",
					fontSize: "13px",
					fontWeight: 200,
					fontStyle: "italic",
				}}
			>
				{errors[name] && errors[name].message}
			</p>
		</div>
	);
};

// export function RadioInput({
// 	name,
// 	label,
// 	register,
// 	type,
// 	className,
// 	errors,
// 	...rest
// }) {
// 	const id = useId();
// 	return (
// 		<div className="form-check form-check-inline">
// 			<label className="form-check-label" htmlFor={id}>
// 				Non
// 			</label>
// 			<input id={id} type="radio" className="form-check-input" />
// 		</div>
// 	);
// }

export function BooleanInput({
	control,
	name,
	unregisterTruthy = null,
	unregisterFlasy = null,
}) {
	const falsyId = useId();
	const truthyId = useId();

	return (
		<Controller
			control={control}
			name={name}
			render={({ field: { onChange, onBlur, value, ref } }) => (
				<>
					<div className="form-check form-check-inline">
						<label className="form-check-label" htmlFor={truthyId}>
							Oui
						</label>
						<input
							id={truthyId}
							type="radio"
							className="form-check-input"
							onBlur={onBlur}
							onChange={() => {
								onChange(true);
								if (unregisterTruthy) unregisterTruthy();
							}}
							checked={value === true}
							inputRef={ref}
						/>
					</div>
					<div className="form-check form-check-inline">
						<label className="form-check-label" htmlFor={falsyId}>
							Non
						</label>
						<input
							id={falsyId}
							type="radio"
							className="form-check-input"
							onBlur={onBlur}
							onChange={() => {
								onChange(false);
								if (unregisterFlasy) unregisterFlasy();
							}}
							checked={value === false}
							inputRef={ref}
						/>
					</div>
				</>
			)}
		/>
	);
}

export function RadioInput({ control, name, radioGroup }) {
	return (
		<Controller
			control={control}
			name={name}
			render={({ field: { onChange, onBlur, value, ref } }) =>
				radioGroup?.map((el) => {
					return (
						<div className="form-check form-check-inline">
							<label
								className="form-check-label"
								htmlFor={el?.id}
							>
								{el?.label}
							</label>
							<input
								id={el?.id}
								type="radio"
								className="form-check-input"
								onBlur={onBlur}
								onChange={() => onChange(el?.value)}
								checked={value === el?.value}
								inputRef={ref}
							/>
						</div>
					);
				})
			}
		/>
	);
}

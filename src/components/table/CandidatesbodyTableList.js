import React from "react";
import RowList from "./CandidateRow/index";
export const CandidatesbodyTableList = ({ data }) => {
    console.log('data',data);
	return (
		<tbody>
			{data?.map((el, index) => {
			return (    
				<RowList el={el} key={index}/>
			)	
			})}
		</tbody>
	);
};


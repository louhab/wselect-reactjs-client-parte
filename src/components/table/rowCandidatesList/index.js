/* eslint-disable no-unreachable */
import React from "react";
import dayjs from "dayjs";

const CandidatesRowList = ({Row, Key}) => {
    return (
        <tr key={`RowTable${Key}`} >
	  	<td>
		  	<a href="ws-entreprise-detail-candidate.html">
                <div className="pxp-company-dashboard-job-title">{Row.firstName} {Row.lastName } </div>
                <div className="pxp-company-dashboard-job-location"><span className="fa fa-globe me-1"></span>{Row.city} </div>
            </a>
		</td>
    	<td ><div className="pxp-company-dashboard-candidate-title">{Row.phone}</div></td>
		<td><div className="pxp-company-dashboard-candidate-location">{Row?.offer[0].offer.title}</div></td>
		<td><div className="pxp-company-dashboard-job-status">{Row.status ==='pass' ? <span className="badge rounded-pill bg-success">Confirmé</span> : <span className="badge rounded-pill bg-danger">Réfusé</span> }</div></td>

		<td><div className="pxp-company-dashboard-candidate-location">  {dayjs(Row.createdAt).format("YYYY/MM/DD")} </div></td>
		<td>
			<div className="pxp-dashboard-table-options">
				<ul className="list-unstyled">
					<li><button title="View profile"><span className="fa fa-eye"></span></button></li>
					<li><button title="Approve"><span className="fa fa-check"></span></button></li>
					<li><button title="Reject"><span className="fa fa-ban"></span></button></li>
            	</ul>
			</div>
		</td>
        </tr>

    )
}

export default CandidatesRowList;
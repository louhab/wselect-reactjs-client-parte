/* eslint-disable no-unreachable */
import React from "react";
const CandidatesRow = ({el, Key}) => {
    return (
	<tr  key={`RowTable${Key}`}>
                               
		<td ><div className="pxp-company-dashboard-candidate-name">{el.firstName } {el.lastName}</div></td>
		<td ><div className="pxp-company-dashboard-candidate-title">{el.phone}</div></td>
		<td><div className="pxp-company-dashboard-candidate-location"><span className="fa fa-globe"></span>{ el.offer[0].offer.title}</div></td>
		<td><div className="pxp-company-dashboard-candidate-location"><span className="fa fa-globe"></span>{el?.address }</div></td>
		<td>
			<div className="pxp-dashboard-table-options">
				<ul className="list-unstyled">
					<li><button title="View profile"><span className="fa fa-eye"></span></button></li>
				</ul>
			</div>
		</td>
	</tr>
    )
}

export default CandidatesRow;
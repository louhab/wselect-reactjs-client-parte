import React from "react";
import dayjs from "dayjs";
import toast, { Toaster } from "react-hot-toast";
import { instance } from "../../lib/axios";
export const CandidatesTableList = ({ data,setComponent }) => {
	async function handlStatus(id,newState ) {
		let status = ''
		if(newState === 'Confirmé') {
			status = 'pass'
		}else {
			status = 'fail'
		}

		instance
			.put(`/candidates/${id}`, {
				status:status
			})
			.then((res) => {
				 toast.success(`le condidat est ${newState}`);
				 window.location.reload(false)
			})
			.catch((err) => {
				console.log(err);
			});
	}
    return (

        <tbody >
			<td>
			<Toaster position="bottom-center" reverseOrder={false} />
			</td>
            {data?.map((elem, index) => {
                const Row = elem
                return <tr key={`RowTable${index}`} >
				<td>
					<a href="ws-entreprise-detail-candidate.html">
					  <div className="pxp-company-dashboard-job-title">{Row.firstName} {Row.lastName } </div>
					  <div className="pxp-company-dashboard-job-location"><span className="fa fa-globe me-1"></span>{Row.address} </div>
				  </a>
			  </td>
			  <td ><div className="pxp-company-dashboard-candidate-title">{Row.phone}</div></td>
			  <td><div className="pxp-company-dashboard-candidate-location">{Row?.offer[0].offer.title}</div></td>
			  <td><div className="pxp-company-dashboard-job-status">{Row.status ==='pass' ? <span className="badge rounded-pill bg-success">Confirmé</span> : <span className="badge rounded-pill bg-danger">Réfusé</span> }</div></td>
	  
			  <td><div className="pxp-company-dashboard-candidate-location">  {dayjs(Row.createdAt).format("YYYY/MM/DD")} </div></td>
			  <td>
				  <div className="pxp-dashboard-table-options">
					  <ul className="list-unstyled">
						  <li><button title="Consulter" onClick={()=>setComponent(`${elem?.id}`)} ><span className="fa fa-eye"></span></button></li>
						<li>
							{
								Row.status === 'pass' ?
								<button title="Réfusé" onClick={()=>{handlStatus(Row.id ,'Réfusé')}}><span className="fa fa-ban"></span></button>
								: <button title="Confirmé"  onClick={()=>{handlStatus(Row.id,'Confirmé' )}}><span className="fa fa-check"></span></button>
							}
						</li>
					  </ul>
				  </div>
			  </td>
			  </tr>
            })}
        </tbody>
    );
}

import React from "react";

export const CandidatesTable = ({ data,setComponent }) => {
    return (
        <tbody >
            {data?.map((elem, index) => {
                return <tr  key={`RowTable${index}`}>
                               
				<td ><div className="pxp-company-dashboard-candidate-name">{elem?.firstName } {elem?.lastName}</div></td>
				<td ><div className="pxp-company-dashboard-candidate-title">{elem?.phone}</div></td>
				<td><div className="pxp-company-dashboard-candidate-location"><span className="fa fa-globe"></span>{elem?.offer[0]?.offer?.title}</div></td>
				<td><div className="pxp-company-dashboard-candidate-location"><span className="fa fa-globe"></span>{elem?.address}</div></td>
				<td>
					<div className="pxp-dashboard-table-options">
						<ul className="list-unstyled">
							<li><button title="Consulter" ><span className="fa fa-eye" onClick={()=>setComponent(`${elem?.id}`)}></span></button></li>
						</ul>
					</div>
				</td>
			</tr>
            })}
        </tbody>
    );
}

import React from "react";
// import LogoOne from "../assets/images/hero-logo-1.svg";
// import LogoTwo from "../assets/images/hero-logo-2.svg";
// import LogoThree from "../assets/images/hero-logo-3.svg";
// import LogoFour from "../assets/images/hero-logo-4.svg";
// import LogoFive from "../assets/images/hero-logo-5.svg";
// import LogoSix from "../assets/images/hero-logo-6.svg";

function Hero() {
    return (
        <>
            <section className="pxp-hero vh-100" style={{ backgroundColor: 'var(--pxpSecondaryColor)' }}>
                <div className="pxp-hero-caption">
                    <div className="pxp-container">
                        <div className="row pxp-pl-80 align-items-center justify-content-between">
                            <div className="col-12 col-xl-6 col-xxl-5">

                                <h1 >Trouvez <span style={{ color: "var(--pxpMainColor)" }}>l'emploi</span> idéal pour vous</h1>
                                 <div className="pxp-hero-subtitle mt-3 mt-lg-4">Découvrez de nombreuses opportunités professionnelles parmi une vaste sélection d'offres d'emploi.</div>

                               <div className="pxp-hero-form pxp-hero-form-round pxp-bigger mt-3 mt-lg-4">
                                    <form className="row gx-3 align-items-center" action="jobs-list-1.html">
                                        <button>Déposer votre CV </button>
                                    </form>
                                </div> 

                                <div className="pxp-hero-searches-container">
                                    <div className="pxp-hero-searches-label">Thèmes de recherche courants</div>
                                    <div className="pxp-hero-searches">
                                        <div className="pxp-hero-searches-items">
                                            <a href="#">Agriculteur</a>
                                            <a href="#">Génie civil</a>
                                            <a href="#">Electricité</a>
                                            <a href="#">Design</a>
                                            <a href="#">Enseignant</a>
                                            <a href="#">Comptabilité</a>
                                            <a href="#">Chef de cuisine</a>
                                            <a href="#">Styliste</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="pxp-hero-right-bg-card pxp-with-image pxp-has-animation pxp-animate" style={{ backgroundImage: `var(--backgroundImage)` }}></div>
            </section>

        </>
    )
}

export default Hero;

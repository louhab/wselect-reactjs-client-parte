import React from "react";
import { Link } from "react-router-dom";
import { COLORS } from "../../utils";
// import Logo from "../../assets/images/Logo-HR-Globe-Profil.png";
import Logo from "../../assets/images/logo-transparant.png";

export default function Footer() {
	return (
		<footer className="pxp-main-footer">
			<div
				className="pxp-main-footer-top pt-5 pb-5"
				style={{ backgroundColor: COLORS.customMainColorLight }}
			>
				<div className="pxp-container">
					<div className="row">
						<div className="col-lg-6 col-xl-5 col-xxl-4 mb-4">
							<div className="pxp-footer-logo">
								<Link to="/" className="pxp-animate">
									<img
										style={{ height: "37px" }}
										src={Logo}
										alt="logo"
									/>
								</Link>
							</div>
							<div className="pxp-footer-section mt-3 mt-md-4">
								
							</div>
							<div className="mt-3 mt-md-4 pxp-footer-section">
								<div className="pxp-footer-text">
								<div className="pxp-footer-phone">
									+1 514 385-0202
								</div>
									107-10150 avenue papineau
									<br />
									Québec, Canada
									<br />
									info@wselect.ca
									<br />
								</div>
							</div>
						</div>
						<div className="col-lg-6 col-xl-7 col-xxl-8">
							<div className="row">
								<div className="col-md-6 col-xl-6 col-xxl-6 mb-4">
									<div className="pxp-footer-section">
										<h3>Nous sommes là pour vous</h3>
										<p>
										Pour trouver des réponses aux questions les plus fréquentes, vous pouvez nous contacter directement. Nous sommes là pour vous assister.
										</p>
										<a className="btn rounded-pill pxp-card-btn" href="/contact">Contactez nous</a>
									</div>
								</div>
								<div className="col-md-3 col-xl-3 col-xxl-3 mb-4">
                                    <div className="pxp-footer-section">
                                        <h3>Candidatures</h3>
                                        <ul className="pxp-footer-list">
                                            <li><a href="#">Parcourir les emplois</a></li>
                                            <li><a href="#">Conseils de carrière</a></li>
                                            <li><a href="#">Salaires</a></li>
                                            <li><a href="#">Déposer mon CV</a></li>
                                        </ul>
                                    </div>
                                </div>
								<div className="col-md-3 col-xl-3 col-xxl-3 mb-4">
                                    <div className="pxp-footer-section">
                                        <h3>Ressources</h3>
                                        <ul className="pxp-footer-list">
                                            <li>
											<Link
												to="/Conditions-utilisation"
												className="ishover"
											>
												Conditions d'utilisation
											</Link>
											</li>
                                            <li>
											<Link
											to="/Conditions-de-ventes"
											className="ishover"
										>
											Conditions de ventes
										</Link>
												</li>
											<li>
											<Link to="/employer-login" className="ishover">
												Connexion Entreprise
											</Link>
											</li>
											<li>
											<Link
												to="/employer-sign-up"
												className="ishover"
											>
												Inscription Entreprise
											</Link>
											</li>
                                        </ul>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div
				className="pxp-main-footer-bottom"
			>
				<div className="pxp-container">
					<div className="row justify-content-between align-items-center">
						<div className="col-lg-auto">
							<div className="pxp-footer-copyright pxp-text-light">
								<p style={{ fontSize: "12px" }}>
									© {new Date().getFullYear()} World select.
									All Right Reserved - crafted <i className="fa fa-heart text-danger"></i> by <a href="https://boom-digital.ma/" target="_blank" rel="noreferrer">Boom Digital</a>
								</p>
							</div>
						</div>
						
						<div className="col-lg-auto">
							
							<div className="pxp-footer-social mt-3 mt-lg-0">
								<ul className="list-unstyled">
									<li>
										<a
											href="https://www.facebook.com/wselectca"
											target="_blank"
											rel="noreferrer"
										>
											<span className="fa fa-linkedin"></span>
										</a>
									</li>
									<li>
										<a
											href="https://www.facebook.com/wselectca"
											target="_blank"
											rel="noreferrer"
										>
											<span className="fa fa-facebook"></span>
										</a>
									</li>
									<li>
										<a
											href="https://www.instagram.com/wselectca"
											target="_blank"
											rel="noreferrer"
										>
											<span className="fa fa-instagram"></span>
										</a>
									</li>
									<li>
										<a
											href="mailto: info@wselect.ca"
											target="_blank"
											rel="noreferrer"
										>
											<span className="fa fa-envelope"></span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
	);
}

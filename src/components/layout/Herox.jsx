import React from "react";
// import LogoOne from "../assets/images/hero-logo-1.svg";
// import LogoTwo from "../assets/images/hero-logo-2.svg";
// import LogoThree from "../assets/images/hero-logo-3.svg";
// import LogoFour from "../assets/images/hero-logo-4.svg";
// import LogoFive from "../assets/images/hero-logo-5.svg";
// import LogoSix from "../assets/images/hero-logo-6.svg";
import HeroImage from "../../assets/images/Services-condidat.jpg";

function Hero() {
	return (
		<section
			className="pxp-hero vh-100"
			style={{ backgroundColor: "#fff" }}
		>
			<div className="pxp-hero-caption">
				<div className="pxp-container">
					<div className="row pxp-pl-80 align-items-center justify-content-between">
						<div className="col-12 col-xl-6 col-xxl-5">
							<h1 style={{
								// fontFamily: "barlow",
								fontWeight: "bold",
								fontSize: "60px",
								color: "#2D3E83"
							}}>
								Trouvez{" "}
								<span
									style={{
										// color: "#a61816",
										color: "#fdc800",
										display: "inline-block",
									}}
								>
									l'emploi{" "}
								</span>
								{" "}qui vous convient
							</h1>
							{/* TBD */}
							{/* <div className="pxp-hero-subtitle mt-3 mt-lg-4">
								Search your career opportunity through{" "}
								<strong>12,800</strong> jobs
							</div> */}
						</div>
						<div className="d-none d-xl-block col-xl-5 position-relative">
							<div
								className="pxp-hero-cards-container pxp-animate-cards pxp-mouse-move"
								data-speed="160"
							>
								<div
									className="pxp-hero-card pxp-cover pxp-cover-top"
									style={{
										backgroundImage: `url(${HeroImage})`,
										backgroundPosition: "-247px",
									}}
								></div>
								<div className="pxp-hero-card-dark"></div>
								<div className="pxp-hero-card-light"></div>
							</div>

							<div
								className="pxp-hero-card-info-container pxp-mouse-move"
								data-speed="60"
							>
								<div className="pxp-hero-card-info pxp-animate-bounce">
									<div className="pxp-hero-card-info-item">
										<div className="pxp-hero-card-info-item-number">
											286<span>job offers</span>
										</div>
										<div className="pxp-hero-card-info-item-description">
											in Business Development
										</div>
									</div>
									<div className="pxp-hero-card-info-item">
										<div className="pxp-hero-card-info-item-number">
											154<span>job offers</span>
										</div>
										<div className="pxp-hero-card-info-item-description">
											in Marketing & Communication
										</div>
									</div>
									<div className="pxp-hero-card-info-item">
										<div className="pxp-hero-card-info-item-number">
											319<span>job offers</span>
										</div>
										<div className="pxp-hero-card-info-item-description">
											in Human Resources
										</div>
									</div>
									<div className="pxp-hero-card-info-item">
										<div className="pxp-hero-card-info-item-number">
											120<span>job offers</span>
										</div>
										<div className="pxp-hero-card-info-item-description">
											in Project Management
										</div>
									</div>
									<div className="pxp-hero-card-info-item">
										<div className="pxp-hero-card-info-item-number">
											176<span>job offers</span>
										</div>
										<div className="pxp-hero-card-info-item-description">
											in Customer Service
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div className="pxp-hero-logos-carousel-container">
				<div className="pxp-container">
					<div className="row pxp-pl-80">
						<div className="col-12 col-xl-6">
							{/* <div className="pxp-hero-logos-carousel owl-carousel">
								<img
									className="custom-carousel"
									src={LogoOne}
									alt="Logo 1"
								/>
								<img
									className="custom-carousel"
									src={LogoTwo}
									alt="Logo 2"
								/>
								<img
									className="custom-carousel"
									src={LogoThree}
									alt="Logo 3"
								/>
								<img
									className="custom-carousel"
									src={LogoFour}
									alt="Logo 4"
								/>
								<img
									className="custom-carousel"
									src={LogoFive}
									alt="Logo 5"
								/>
								<img
									className="custom-carousel"
									src={LogoSix}
									alt="Logo 6"
								/>
							</div> */}
						</div>
					</div>
				</div>
			</div>

			<div className="pxp-hero-right-bg-card pxp-has-animation"></div>
		</section>
	);
}

export default Hero;

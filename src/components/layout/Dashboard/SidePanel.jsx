import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import onContentScroll from "../../../lib/main/onContentScroll";
import handlePreloader from "../../../lib/main/handlePreloader";
import windowResizeHandler from "../../../lib/main/windowResizeHandler";
import $ from "jquery";
import Logo from "../../../assets/images/logo-transparant.png";

const SidePanel = ({ show, setShow, setPosition, position, panel }) => {

	useEffect(() => {
		window.onscroll = function () {
			onContentScroll();
		};
		handlePreloader();

		windowResizeHandler();

		$(window).resize(function () {
			windowResizeHandler();
		});

		window.scrollTo(0, 0);
	}, []);

	return (
		<>
			{/* FIXME => side pannel */}
			{show ? (
				<div
					className="offcanvas-backdrop fade show d-lg-none"
					style={{ zIndex: 50 }}
				></div>
			) : null}

			<div
				className={`pxp-dashboard-side-panel ${show && "d-block"}`}
				style={{ transition: "all 0.3s ease-out-in" , backgroundColor: '#FFF8EC'}}
			>
				<div className="position-fixed">
					<div
						className="d-flex justify-content-between align-items-center"
						style={{ width: "250px" }}
					>
						<div className="pxp-logo">
							<Link to="/" className="pxp-animate ">
								<img
									style={{ height: "37px" }}
									src={Logo}
									alt="logo"
								/>
							</Link>
						</div>
						<button
							type="button"
							className="btn-close text-reset d-lg-none"
							data-bs-dismiss="oncanvas"
							aria-label="Close"
							onClick={() => {
								setShow(0);
							}}
						></button>
					</div>

					<nav className="mt-3 mt-lg-4 d-flex justify-content-between flex-column position-fixed">
						<div className="pxp-dashboard-side-label"></div>
						<ul className="list-unstyled">
							{panel.map((elem, index) => {
								
								const temp =
									index === position ? "pxp-active" : "";
								return (
									<li
										style={{
											opacity:
												elem?.status === 0
													? "0.4"
													: "1",
										}}
										key={index}
										className={temp}
									>
										<Link
											onClick={() => {
												elem?.status === 1 &&
													setPosition(index);
											}}
											to={""}
										>
											<span
												className={elem.spanClass}
											></span>
											{elem.title}
										</Link>
									</li>
								);
							})}
						</ul>
						{/* <div className="pxp-dashboard-side-label mt-3 mt-lg-4">
							Insights
						</div> */}
						{/* <ul className="list-unstyled">
							<li>
								<Link
									onClick={() => {
										setPosition(20);
									}}
									to={""}
									className="d-flex justify-content-between align-items-center"
								>
									<div>
										<span className="fa fa-envelope-o"></span>
										Inbox
									</div>
								</Link>
							</li>
						</ul> */}
					</nav>
				</div>
			</div>
		</>
	);
};

export default SidePanel;

import React, { useEffect, useState,useContext } from "react";
import { instance } from '../../../lib/axios';
import { CandidatesTable } from "../../table/CandidatesTable";
import CandidatDetails from "../../../pages/Dashboard/candidate/CandidatDetails";
import { AuthContext } from "../../../context/Auth";

function Content() {
	const { auth } = useContext(AuthContext);

	const [candidates, setCandidates] = useState([]);
	const [waiting, setWaiting] = useState(true);
	const [candidatesCount,setCandidatesCount] = useState(0);
	const [offerCount, setOfferCount] = useState(0)
	let [component , setComponent]= useState('Dashboard');
	useEffect(()=>{
		instance.get(`employers/${auth.id}`)
		.then((response)=>{
			setOfferCount(response.data.results.Offer.length)
		})

	})
	useEffect(() => {
		instance
			.get(`/candidates?skip=`+0+`&take=`+4)
			.then((res) => {
					setCandidatesCount(res.data.results.candidatesCount)
					setCandidates(res.data.results.candidates)
					setWaiting(false)
			})
			.catch((err) => {
				console.log(err);
			});
	}, []);
	return (
		<>
		    <div className="pxp-dashboard-content-details"  >
                
				{
					waiting ? 
					<div className="pxp-dashboard-content-details" >
								<div className="text-center">
									<div className="spinner-border" role="status">
										<span className="sr-only">Loading...</span>
									</div>
								</div>
						</div>
					:  component=== 'Dashboard' ?
					<div>
						<h1>Dashboard</h1>
					<p className="pxp-text-light">Bienvenue sur Wselect!</p>
                <div className="row mt-4 mt-lg-5 align-items-center">
                    <div className="col-sm-6 col-md-6 col-xxl-6">
                        <div className="pxp-dashboard-stats-card bg-primary bg-opacity-10 mb-3 mb-xxl-0" >
                            <div className="pxp-dashboard-stats-card-icon text-primary">
                                <span className="fa fa-file-text-o"></span>
                            </div>
                            <div className="pxp-dashboard-stats-card-info">
                                <div className="pxp-dashboard-stats-card-info-number">{offerCount }</div>
                                <div className="pxp-dashboard-stats-card-info-text pxp-text-light">Offres </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-6 col-md-6 col-xxl-6">
                        <div className="pxp-dashboard-stats-card bg-success bg-opacity-10 mb-3 mb-xxl-0">
                            <div className="pxp-dashboard-stats-card-icon text-success">
                                <span className="fa fa-user-circle-o"></span>
                            </div>
                            <div className="pxp-dashboard-stats-card-info">
                                <div className="pxp-dashboard-stats-card-info-number">{candidatesCount}</div>
                                <div className="pxp-dashboard-stats-card-info-text pxp-text-light">Candidatures</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mt-4 mt-lg-5">
                  {
					candidates.candidates.length === 0 ?
						<div style={{textAlign:'center'}}>
							<span>Aucun candidature  Pour le moment</span>
						</div>
					: 
					<div>
					<h2>Candidatures récente</h2>
                    <div className="table-responsive">
						<table className="table align-middle">
							<CandidatesTable data={candidates.candidates} setComponent={setComponent} />
						</table>
                    </div>
					</div>
				  }
                </div>
					</div>
					: <CandidatDetails id={component} setComponent={setComponent} />
				}
            </div>

			<footer>
				<div className="pxp-footer-copyright pxp-text-light">
					© {new Date().getFullYear()} World select. All Right
					Reserved.
				</div>
			</footer>
		</>
	);
}

export default Content;

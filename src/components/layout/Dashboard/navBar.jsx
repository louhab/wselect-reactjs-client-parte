import React, { useContext } from "react";
import { Link } from "react-router-dom";
import ComLogo from "../../../assets/images/company-logo-1.png";
import { AuthContext } from "../../../context/Auth";
const NavBar = ({ setShow, candidate }) => {
	const { auth } = useContext(AuthContext);
	const { logout } = useContext(AuthContext);
	return (
		<div
			style={{
				top: 5,
				padding: 30,
				paddingRight: 60,
				right: 0,
				zIndex: 0,
			}}
			className="pxp-dashboard-content-header "
		>
			<div className="pxp-nav-trigger navbar pxp-is-dashboard d-lg-none">
				<a
					onClick={() => {
						setShow(1);
					}}
				>
					<div className="pxp-line-1"></div>
					<div className="pxp-line-2"></div>
					<div className="pxp-line-3"></div>
				</a>
			</div>

			<nav className="pxp-user-nav pxp-on-light">
				{/* <Link
                    to="company-dashboard-new-job.html"
                    className="btn rounded-pill pxp-nav-btn"
                >
                    Post a Job
                </Link> */}
				{/* <div className="dropdown pxp-user-nav-dropdown pxp-user-notifications">
                    <Link
                        role="button"
                        className="dropdown-toggle"
                        data-bs-toggle="dropdown"
                        to={"#"}
                    >
                        <span className="fa fa-bell-o"></span>
                        <div className="pxp-user-notifications-counter">
                            5
                        </div>
                    </Link>
                    <ul className="dropdown-menu dropdown-menu-end">
                        <li>
                            <Link
                                className="dropdown-item"
                                to="company-dashboard-notifications.html"
                            >
                                <strong>Scott Goodwin</strong> applied for{" "}
                                <strong>Software Engineer</strong>.{" "}
                                <span className="pxp-is-time">20m</span>
                            </Link>
                        </li>
                        <li>
                            <Link
                                className="dropdown-item"
                                to="company-dashboard-notifications.html"
                            >
                                <strong>Alayna Becker</strong> sent you a
                                message.{" "}
                                <span className="pxp-is-time">1h</span>
                            </Link>
                        </li>
                        <li>
                            <Link
                                className="dropdown-item"
                                to="company-dashboard-notifications.html"
                            >
                                <strong>Erika Tillman</strong> applied for{" "}
                                <strong>Team Leader</strong>.{" "}
                                <span className="pxp-is-time">2h</span>
                            </Link>
                        </li>
                        <li>
                            <Link
                                className="dropdown-item"
                                to="company-dashboard-notifications.html"
                            >
                                <strong>Scott Goodwin</strong> applied for{" "}
                                <strong>Software Engineer</strong>.{" "}
                                <span className="pxp-is-time">5h</span>
                            </Link>
                        </li>
                        <li>
                            <Link
                                className="dropdown-item"
                                to="company-dashboard-notifications.html"
                            >
                                <strong>Alayna Becker</strong> sent you a
                                message.{" "}
                                <span className="pxp-is-time">1d</span>
                            </Link>
                        </li>
                        <li>
                            <Link
                                className="dropdown-item"
                                to="company-dashboard-notifications.html"
                            >
                                <strong>Erika Tillman</strong> applied for{" "}
                                <strong>Software Engineer</strong>.{" "}
                                <span className="pxp-is-time">3d</span>
                            </Link>
                        </li>
                        <li>
                            <hr className="dropdown-divider" />
                        </li>
                        <li>
                            <Link
                                className="dropdown-item pxp-link"
                                to="company-dashboard-notifications.html"
                            >
                                Read All
                            </Link>
                        </li>
                    </ul>
                </div> */}
				<div className="dropdown pxp-user-nav-dropdown">
					<div
						// role="button"
						className="dropdown-toggle"
						data-bs-toggle="dropdown"
						// to={"#"}
						style={{
							cursor: "pointer",
						}}
					>
						<div
							className="pxp-user-nav-avatar pxp-cover"
							style={{
								backgroundImage: `url(${ComLogo})`,
							}}
						>
							<Link
								role="button"
								className="text-dark"
								data-bs-toggle="offcanvas"
								data-bs-target="#pxpMobileNav"
								aria-controls="pxpMobileNav"
								to={"#"}
							>
								<div className="pxp-line-1"></div>
								<div className="pxp-line-2"></div>
								<div className="pxp-line-3"></div>
							</Link>
						</div>
						<div className="pxp-user-nav-name d-none d-md-block ">
							{auth.user?.firstName}{auth.user?.lastName}{auth.user?.companyName}
						</div>
					</div>
					<ul className="dropdown-menu dropdown-menu-end">
						<li>
							<Link
								className="dropdown-item"
								to="/"
								onClick={() => {
									localStorage.clear("token");
									logout();
								}}
							>
								Se déconnecter
							</Link>
						</li>
					</ul>
				</div>
			</nav>
		</div>
	);
};

export default NavBar;

import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import onContentScroll from "../../lib/main/onContentScroll";
import handlePreloader from "../../lib/main/handlePreloader";
import windowResizeHandler from "../../lib/main/windowResizeHandler";
import $ from "jquery";
import PreLoader from "../PreLoader";

export default function MinimalHeader({ logo, extraNav }) {
	useEffect(() => {
		window.onscroll = function () {
			onContentScroll();
		};

		handlePreloader();

		windowResizeHandler();

		$(window).resize(function () {
			windowResizeHandler();
		});

		window.scrollTo(0, 0);
	}, []);
	return (
		<>
			<PreLoader />
			<header className="pxp-header fixed-top pxp-no-bg">
				<div className="pxp-container">
					<div
						style={{
							display: "flex",
							flexDirection: "row",
							alignItems: "center",
							height: "80px",
							justifyContent: "space-between",
						}}
					>
						<div className="pxp-logo">
							<Link to="/" className="pxp-animate">
								<img
									style={{ height: "37px" }}
									src={logo}
									alt="logo"
								/>
							</Link>
						</div>
						<div className="pxp-nav-trigger navbar d-xl-none flex-fill">
							<Link
								to={"#"}
								role="button"
								data-bs-toggle="offcanvas"
								data-bs-target="#pxpMobileNav"
								aria-controls="pxpMobileNav"
							>
								<div className="pxp-line-1"></div>
								<div className="pxp-line-2"></div>
								<div className="pxp-line-3"></div>
							</Link>
							<div
								className="offcanvas offcanvas-start pxp-nav-mobile-container"
								tabIndex={"-1"}
								id="pxpMobileNav"
							>
								<div className="offcanvas-header">
									<div className="pxp-logo">
										<Link to="/" className="pxp-animate">
											<img
												style={{ height: "37px" }}
												src={logo}
												alt="logo"
											/>
										</Link>
									</div>
									<button
										type="button"
										className="btn-close text-reset"
										data-bs-dismiss="offcanvas"
										aria-label="Close"
									></button>
								</div>
							</div>
						</div>
						<nav className={extraNav}>
							<Link
								to="/sign-up"
								className="btn rounded-pill pxp-nav-btn"
								
							>
								S'inscrire
							</Link>
							<Link
								className="btn rounded-pill pxp-user-nav-trigger pxp-on-light"
								to="/login"
								
							>
								Se connecter
							</Link>
						</nav>
					</div>
				</div>
			</header>
		</>
	);
}

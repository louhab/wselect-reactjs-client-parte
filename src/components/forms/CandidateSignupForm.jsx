import React, { useState } from "react";
import { instance } from "../../lib/axios";
import toast, { Toaster } from "react-hot-toast";

const INIT_FORM = {
	firstName: "",
	lastName: "",
	phone: "",
	email: "",
	password: "",
};

export default function CandidateSignupForm() {
	const [form, setFrom] = useState(INIT_FORM);

	function handleChange({ target: { name, value } }) {
		setFrom((init) => ({ ...init, [name]: value }));
	}

	async function handleSubmit(e) {
		try {
			e.preventDefault();

			await instance.post("/candidates/sign-up", form);

			setTimeout(() => {
				toast.success("Bravo!");
				setFrom(INIT_FORM);
			}, 500);
		} catch (e) {
			console.log("response" in e);
			if (
				"response" in e &&
				"data" in e?.response &&
				(e?.response?.status === 409 || e?.response?.status === 401) &&
				e?.response?.data?.success === false
			) {
				const { error } = e?.response?.data;
				toast.error(error.message, {
					style: {
						borderRadius: "10px",
						background: "#333",
						color: "#fff",
						fontSize: "14px",
					},
				});
			} else {
				toast.error("Merci de remplir le formulaire", {
					style: {
						borderRadius: "10px",
						background: "#333",
						color: "#fff",
						fontSize: "14px",
					},
				});
			}
		}
	}

	return (
		<section className="mt-100 pxp-no-hero">
			<Toaster position="bottom-center" reverseOrder={false} />
			<div className="pxp-container">
				<div className="row pb-100 justify-content-center pxp-animate-in pxp-animate-in-top pxp-in">
					<div className="col-lg-6 col-xxl-4">
						<div className="pxp-contact-us-form pxp-has-animation pxp-animate">
							<h2 className="pxp-section-h2 text-center">
								Formulaire de Pré candidature
							</h2>
							<p className="pxp-text-light text-center">Get in touch with us</p>
							<form
								className="mt-4 "
								onSubmit={handleSubmit}
								encType="multipart/form-data"
							>
								<div className="mb-3">
									<label
										htmlFor="first-name"
										className="form-label"
									>
										Nom
									</label>
									<input
										name="firstName"
										value={form?.firstName}
										onChange={handleChange}
										type="text"
										className="form-control"
										id="first-name"
										placeholder="Entrez Votre Nom"
									/>
								</div>
								<div className="mb-3">
									<label
										htmlFor="last-name"
										className="form-label"
									>
										Prénom
									</label>
									<input
										name="lastName"
										value={form?.lastName}
										onChange={handleChange}
										type="text"
										className="form-control"
										id="last-name"
										placeholder="Entrez Votre Prénom"
									/>
								</div>
								<div className="mb-3">
									<label
										htmlFor="contact-phone"
										className="form-label"
									>
										Numéro de téléphone
									</label>
									<input
										name="phone"
										value={form?.phone}
										onChange={handleChange}
										type="text"
										className="form-control"
										id="contact-phone"
										placeholder="123-456-789"
									/>
								</div>

								<div className="mb-3">
									<label
										htmlFor="contact-email"
										className="form-label"
									>
										Email
									</label>
									<input
										name="email"
										value={form?.email}
										onChange={handleChange}
										type="text"
										className="form-control"
										id="contact-email"
										placeholder="email@exemple.com"
									/>
								</div>

								<div className="mb-3">
									<label
										htmlFor="password"
										className="form-label"
									>
										Mot de passe
									</label>
									<input
										name="password"
										value={form?.password}
										onChange={handleChange}
										type="password"
										className="form-control"
										id="password"
									/>
								</div>

								<br />
								<button
									className="btn rounded-pill pxp-section-cta d-block"
									type="submit"
									style={{ width: "100%" }}
								>
									S'inscrire
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
}

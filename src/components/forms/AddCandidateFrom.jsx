import React, { useState, useEffect } from "react";
import { instance } from "../../lib/axios";
import SuccessCandidate from "../SuccessCandidate";
import toast, { Toaster } from "react-hot-toast";
import useQuery from "../../lib/useQuery";
import { useNavigate } from "react-router-dom";
import axios from 'axios';
const LEVELS = [
	{
		label: "one",
		value: 1,
	},
	{ label: "two", value: 2 },
	{ label: "three", value: 3 },
	{ label: "four", value: 4 },
	{ label: "five", value: 5 },
	{ label: "six", value: 6 },
	{ label: "seven", value: 7 },
	{ label: "eight", value: 8 },
	{ label: "nine", value: 9 },
	{ label: "ten", value: 10 },
];

const GENDER = [
	{ label: "man", value: "Homme" },
	{ label: "woman", value: "Femme" },
];

const LEVELS_LANG = [
	{
		label: "one",
		value: "Débutant",
	},
	{ label: "four", value: "Moyen" },
	{ label: "seven", value: "Bien" },
	{ label: "ten", value: "Excellent" },
];

const BOOL = [
	{ label: false, value: "Non" },
	{
		label: true,
		value: "Oui",
	},
];

export default function AddCandidateFrom() {
	const navigate = useNavigate();
	const query = useQuery();
	const [submited, setSubmited] = useState(false);
	const [isActive, setIsActive] = useState(false);
	const [diplomes, setDiplomes] = useState([]);
	const [loading, setLoading] = useState(false);
	const [employer, setEmployer] = useState({
		id: null,
		companyName: null,
	});
	const [offre, setOffre] = useState({
		id: null,
		title: null,
	});
	const [form, setFrom] = useState({
		firstName: "",
		lastName: "",
		phone: "",
		diplomasName: "",
		hasDiplomas: "0",
		yearsExp: "one",
		languageLevel: "one",
		email: "",
		gender: "man",
		domaineDactiviteId: "",
		diplomeId: "",
	});
	const [file, setFile] = useState();
	const saveFile = (e) => {
		setFile(e.target.files[0]);
	};
	function handleChange({ target: { name, value } }) {
		setFrom((init) => ({ ...init, [name]: value }));
		if (name === "hasDiplomas") setIsActive(!!Boolean(value));
		if (name === "diplomeId") setIsActive(!!Number(value));
	}
	async function handleSubmit(e) {		
	try {
		setLoading(true);
		e.preventDefault(); 
		const formData = new FormData();
		for (const key in form) {
		 		formData.append(key, form[key]);
		}
		formData.append("employerId",Number(employer.id))
		formData.append("offerId",Number(offre.id));
		formData.append("cv_file", file);
		formData.append("domaineDactiviteId",null)
		await instance.post("/precandidates", formData);
			setSubmited(true);
			toast.success("Bravo!");
			setLoading(false);
	}
	catch(e){
		console.log(e)
	}
	}
	const fetchOffer = ()=>	 {
		var url = window.location.href
		const urlObj = new URL(url);
		const uuid = urlObj.searchParams.get("uuid");
		let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTIsInR5cGUiOiJjIiwiaWF0IjoxNjg0MTQ3MTE5LCJhdWQiOiIxMiIsImlzcyI6IkJvbW1lcnMifQ.VuI3vSYSHAH2A59931Jk2BvA_3RCI_UxI2p5ivWsjM4';
		let urlToGetOffer = 'http://64.226.123.201:1337/api/v1/offers/uuid/'+uuid;
		axios.get(urlToGetOffer, {
				headers: {
					'Authorization': `Bearer ${token}`
				}
			})
			.then((response)=>{
				setOffre({
						id: response.data.results.id,
						title:response.data.results.title
						});	
			})
	}
	const fetDiplomes = ()=>{
		let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTIsInR5cGUiOiJjIiwiaWF0IjoxNjg0MTQ3MTE5LCJhdWQiOiIxMiIsImlzcyI6IkJvbW1lcnMifQ.VuI3vSYSHAH2A59931Jk2BvA_3RCI_UxI2p5ivWsjM4';
		let urlToGetDiplomes = 'http://64.226.123.201:1337/api/v1/diplomes' ;
		axios.get(urlToGetDiplomes, {
			headers: {
			  'Authorization': `Bearer ${token}`
			}
		})
		.then((response)=>{
			setDiplomes(response?.data?.results);
		})
	}
	const fetchEmployer = ()=>{
		var url = window.location.href
		const urlObj = new URL(url);
		const uuid = urlObj.searchParams.get("employerUuid");
		let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTIsInR5cGUiOiJjIiwiaWF0IjoxNjg0MTQ3MTE5LCJhdWQiOiIxMiIsImlzcyI6IkJvbW1lcnMifQ.VuI3vSYSHAH2A59931Jk2BvA_3RCI_UxI2p5ivWsjM4';
		let urlToGetOffer = 'http://64.226.123.201:1337/api/v1/employers/uuid/'+uuid;
		axios.get(urlToGetOffer, {
				headers: {
					'Authorization': `Bearer ${token}`
				}
			})
			.then((response)=>{
				setEmployer({
						id         :response.data.results.id,
						companyName:response.data.results.title
						});	
			})
	}
	useEffect(() => {	
		fetchOffer()
	 }, [navigate, query]);
	useEffect(()=>{
		fetchEmployer()
	}, [])
	useEffect(() => {
		fetDiplomes()
	}, []);

	return submited ? (
		<>
			<SuccessCandidate name={form?.contactName} />
			<Toaster position="bottom-center" reverseOrder={false} />
		</>
	) : (
		<section className="mt-100 pxp-no-hero">
			<Toaster position="bottom-center" reverseOrder={false} />
			<div className="pxp-container">
				<div className="row pb-100 justify-content-center pxp-animate-in pxp-animate-in-top pxp-in">
					<div className="col-lg-6">
						<div className="pxp-contact-us-form pxp-has-animation pxp-animate">
							<h2 className="pxp-section-h2 text-center">
								Formulaire
								<br /> de Pré-candidature <br />{" "}
								<div style={{ color: "#a61818" }}>
									{offre?.title}
								</div>
							</h2>
							<form
								className="mt-4"
								onSubmit={handleSubmit}
								encType="multipart/form-data"
							>
								<div className="mb-3">
									<label
										htmlFor="first-name"
										className="form-label"
									>
										Nom
									</label>
									<input
										name="firstName"
										value={form?.firstName}
										onChange={handleChange}
										type="text"
										className="form-control"
										id="first-name"
										placeholder="Entrez Votre Nom"
									/>
								</div>
								<div className="mb-3">
									<label
										htmlFor="last-name"
										className="form-label"
									>
										Prénom
									</label>
									<input
										name="lastName"
										value={form?.lastName}
										onChange={handleChange}
										type="text"
										className="form-control"
										id="last-name"
										placeholder="Entrez Votre Prénom"
									/>
								</div>
								<div className="mb-3">
									<label
										htmlFor="years-exp"
										className="form-label"
									>
										Gender
									</label>
									<div className="input-group">
										<select
											className="form-select"
											id="gender"
											onChange={handleChange}
											name="gender"
										>
											{GENDER.map((el, i) => (
												<option
													value={el.label}
													key={i}
												>
													{el.value}
												</option>
											))}
										</select>
									</div>
								</div>
								<div className="mb-3">
									<label
										htmlFor="contact-phone"
										className="form-label"
									>
										Numéro de téléphone
									</label>
									<input
										name="phone"
										value={form?.phone}
										onChange={handleChange}
										type="text"
										className="form-control"
										id="contact-phone"
										placeholder="123-456-789"
									/>
								</div>

								<div className="mb-3">
									<label
										htmlFor="contact-email"
										className="form-label"
									>
										Email
									</label>
									<input
										name="email"
										value={form?.email}
										onChange={handleChange}
										type="text"
										className="form-control"
										id="contact-email"
										placeholder="email@exemple.com"
									/>
								</div>
 
								{/* <div className="mb-3">
									<label
										htmlFor="years-exp"
										className="form-label"
									>
										Domaine D'activite
									</label>
									<div className="input-group">
										<select
											className="form-select"
											id="domaineDactiviteId"
											onChange={handleChange}
											name="domaineDactiviteId"
											value={form.domaineDactiviteId}
										>
											<option disabled value={""}>
												Domaine D'activite
											</option>
											{domaines.map((el, i) => (
												<option value={el.id} key={i}>
													{el.name}
												</option>
											))}
										</select>
									</div>
								</div>  */}

								 <div className="mb-3">
									<label
										htmlFor="years-exp"
										className="form-label"
									>
										Diplômes
									</label>
									<select
										className="form-select"
										id="diplome"
										onChange={handleChange}
										name="diplomeId"
										value={form.diplomeId}
									>
										<option disabled value={""}>
											Diplômes
										</option>
										{diplomes.map((el, i) => (
											<option value={el.id} key={i}>
												{el.name}
											</option>
										))}
									</select>
								</div> 

								<div className="mb-3">
									<label
										htmlFor="diplomas"
										className="form-label"
									>
										Titulaire d'un diplôme de qualification
										?
									</label>
									<div className="input-group">
										<select
											className="form-select"
											id="diplomas"
											onChange={handleChange}
											name="hasDiplomas"
										>
											{BOOL.map((el, i) => (
												<option
													value={el.label}
													key={i}
												>
													{el.value}
												</option>
											))}
										</select>
									</div>
								</div>

								<div
									className="mb-3"
									style={{
										display: isActive ? "block" : "none",
									}}
								>
									<label
										htmlFor="has-diplomas"
										className="form-label"
									>
										Si oui, lequel ?
									</label>
									<input
										name="diplomasName"
										value={form.diplomasName}
										onChange={handleChange}
										type="text"
										className="form-control"
										id="has-diplomas"
										placeholder="Nom de diplome"
									/>
								</div>

								<div className="mb-3">
									<label
										htmlFor="years-exp"
										className="form-label"
									>
										Années d’expériences
									</label>
									<div className="input-group">
										<select
											className="form-select"
											id="years-exp"
											onChange={handleChange}
											name="yearsExp"
										>
											{LEVELS.map((el, i) => (
												<option
													value={el.label}
													key={i}
												>
													{el.value}
												</option>
											))}
										</select>
									</div>
								</div>
								<div className="mb-3">
									<label
										htmlFor="languageLevel"
										className="form-label"
									>
										Niveau de langue en français
									</label>
									<div className="input-group">
										<select
											className="form-select"
											id="languageLevel"
											onChange={handleChange}
											name="languageLevel"
										>
											{LEVELS_LANG.map((el, i) => (
												<option
													value={el.label}
													key={i}
												>
													{el.value}
												</option>
											))}
										</select>
									</div>
								</div>
								<div className="mb-3">
									<label
										htmlFor="languageLevel"
										className="form-label"
									>
										Uploader Votre CV
									</label>
									<div className="pxp-candidate-cover mb-3">
									<label className="file-label" htmlFor="pxp-candidate-cover-choose-file">Selectioner le CV</label>
									<input
										type="file"
										id="pxp-candidate-cover-choose-file"
										className="custom-file-input"
										onChange={saveFile}
										name="cv_file"
									/>
									</div>
								</div>

								<br />
								<button
									className="btn rounded-pill pxp-section-cta d-block"
									type="submit"
									style={{ width: "100%" }}
									// disabled={loading}
								>

									S'inscrire
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
}


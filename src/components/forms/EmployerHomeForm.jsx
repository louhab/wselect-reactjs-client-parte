import React, { useState } from "react";
import { instance } from "../../lib/axios";
import toast, { Toaster } from "react-hot-toast";
import { useNavigate } from "react-router-dom";

export default function EmployerHomeForm() {
	const navigate = useNavigate();
	const [form, setFrom] = useState({
		email: "",
	});

	function handleChange({ target: { name, value } }) {
		setFrom((init) => ({ ...init, [name]: value }));
	}

	async function handleSubmit(e) {
		try {
			e.preventDefault();

			const {
				data: { results },
			} = await instance.post("/employer-forms", form);
			toast.success("Bravo!");
			setTimeout(() => {
				navigate(`/employer-form/${results.uuid}`, { replace: true });
			}, 1000);
		} catch ({
			response: {
				data: { error },
			},
		}) {
			toast.error(error?.message, {
				style: {
					fontSize: "14px",
					borderRadius: "10px",
					background: "#333",
					color: "#fff",
				},
			});
		}
	}

	return (
		<section className="mt-100 pxp-no-hero">
			<Toaster position="bottom-center" reverseOrder={false} />
			<div className="pxp-container">
				<div className="row pb-100 justify-content-center pxp-animate-in pxp-animate-in-top pxp-in">
					<div className="col-lg-6">
						<div className="pxp-contact-us-form pxp-has-animation pxp-animate">
							<div style={{ color: "#a61818" }}>
								<h2
									className="pxp-section-h2 text-center"
									style={{ fontSize: "36px" }}
								>
									Demande d’évaluation de l’impact
									<br /> sur le marché du travail. <br />{" "}
								</h2>
							</div>
							<form className="mt-4" onSubmit={handleSubmit}>
								<div className="mb-3">
									<label
										htmlFor="email"
										className="form-label"
									>
										Courriel
									</label>
									<input
										name="email"
										value={form?.email}
										onChange={handleChange}
										type="text"
										className="form-control"
										id="email"
										placeholder="email@example.com"
									/>
								</div>

								<button
									className="btn rounded-pill pxp-section-cta d-block"
									type="submit"
									style={{ width: "100%" }}
								>
									S'inscrire
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
}

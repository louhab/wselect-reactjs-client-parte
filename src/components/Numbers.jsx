import React from "react";
import Candidature from "./icons/Candidature";
import JobOffers from "./icons/JobOffers";
import Partenaires from "./icons/Partenaires";
import info from "../assets/images/info-section-image.jpg";
import checks from "../assets/images/check.svg";

export default function Numbers() {
	return (
		<section className="pt-100 pb-100">
            <div className="pxp-container">
                <div className="row justify-content-between align-items-center mt-4 mt-md-5">
                    <div className="col-lg-6 col-xxl-5">
                        <div className="pxp-info-fig pxp-animate-in pxp-animate-in-right pxp-in">
                            <div className="pxp-info-fig-image pxp-cover"  style={{
		backgroundImage: `url(${info})`}}></div>
                            <div className="pxp-info-stats">
                                <div className="pxp-info-stats-item pxp-animate-in pxp-animate-bounce pxp-in animate__animated animate__bounceIn">
                                    <div className="pxp-info-stats-item-number">1345<span>Offres d'emploi</span></div>
                                    <div className="pxp-info-stats-item-description">sont disponibles.</div>
                                </div>
                                <div className="pxp-info-stats-item pxp-animate-in pxp-animate-bounce pxp-in animate__animated animate__bounceIn">
                                    <div className="pxp-info-stats-item-number">4800<span>Candidatures </span></div>
                                    <div className="pxp-info-stats-item-description"> ont été traitées. </div>
                                </div>
                                <div className="pxp-info-stats-item pxp-animate-in pxp-animate-bounce pxp-in animate__animated animate__bounceIn">
                                    <div className="pxp-info-stats-item-number">100<span>Partenaires  </span></div>
                                    <div className="pxp-info-stats-item-description">à l'échelle mondiale</div>
                                </div>
                            </div>
                        </div>
						
                    </div>
                    <div className="col-lg-5 col-xxl-6">
                        <div className="pxp-info-caption pxp-animate-in pxp-animate-in-top mt-4 mt-sm-5 mt-lg-0 pxp-in">
                            <h2 className="pxp-section-h2">Chez Wselect<br/> nous sommes déterminés à vous offrir un soutien complet</h2>
                            <p className="pxp-text-light">N'hésitez pas à nous contacter et à saisir cette opportunité de carrière exceptionnelle qui vous attend.</p>
                            <div className="pxp-info-caption-list">
                                <div className="pxp-info-caption-list-item">
								<img
										src={checks}
										alt="-"
									/>
										<span>Partenariat solide avec Clear Path Canada Immigration</span>
                                </div>
                                <div className="pxp-info-caption-list-item">
								<img
										src={checks}
										alt="-"
									/>
										<span>Services personnalisés pour vous assurer une intégration réussie</span>
                                </div>
                                <div className="pxp-info-caption-list-item">
								<img
										src={checks}
										alt="-"
									/>
										<span>Equipe d'experts en immigration</span>
                                </div>
                            </div>
                            <div className="pxp-info-caption-cta">
                                <a href="/about" className="btn rounded-pill pxp-section-cta">En savoir plus<span className="fa fa-angle-right"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

	);
}

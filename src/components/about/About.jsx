import React from "react";
import Globe from "../../assets/images/promo-img-bg.jpg";

export default function About() {
	return (<section className="pt-50 pb-100">
	<div className="pxp-container">
		<div className="pxp-promo-img pxp-cover pt-100 pb-100 pxp-animate-in pxp-animate-in-top pxp-in" style={{
		backgroundImage: `url(${Globe})`}}>
			<div className="row">
				<div className="col-sm-7 col-lg-5">
					<h2 className="pxp-section-h2">Vous trouver un emploi n’est qu’un début !</h2>
					<p className="pxp-text-light">World select vous propose plusieurs perspectives
							d’embauche afin de trouver le poste qui vous
							convient. Une multitude d’offres d’emploi s’offre à
							vous.</p>
							<p className="pxp-text-light">
							Notre mission est de mettre à votre disposition
							notre expertise et vous faire profiter de nos
							services pour vous assurer la meilleure intégration
							au Canada.
						</p>
					<div className="mt-4 mt-md-4">
						<a href="/about" className="btn rounded-pill pxp-section-cta">En savoir plus<span className="fa fa-angle-right"></span></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

	);
}

import React, { useState, useEffect,useContext } from "react";
import {instance} from '../../lib/axios'
import { AuthContext } from "../../context/Auth";
import toast, { Toaster } from "react-hot-toast";

function DocumentsList({candidates,offers,setComponent}) {
	const [Contract , setContract]= useState({
		id : 1,
		name : 'Contrat Wselect',
		path : '8_contrat_wselect.pdf'
	})
	const [documents, setDocuments]= useState([]);
	const [status, setStatus] = useState('');
	const [file , setFile] = useState('');
	const [employerContract, setEmployerContract] = useState([])
	const { auth } = useContext(AuthContext)
	useEffect(() => {
		instance
			.get('/documents')
			.then((res) => {
				setDocuments(res.data.results)
			})
			.catch((err) => {
			});
	}, []);
	useEffect(() => {
		instance
			.get(`/employers/${auth.user.id}`)
			.then((res) => {
				setEmployerContract(res.data.results.EmployerContrat)
				setStatus(res.data.results.status)
			})
			.catch((err) => {
				console.log(err)
			});
	}, []);
	const handleFileChange = (e) => {
		setFile(e.target.files[0])	
	  };
	const handleUpload= ()=>{
		const formData = new FormData();
		formData.append("contract", file);
	    formData.append("employerId",auth.user.id)
		instance.post('employer-contrat', formData)
		.then((response)=>{
			let data  ={
				id :1,
				name: 'Contrat Wselect',
				path :response.data.results.path
			}
			setContract(data) 
			
			setTimeout(() => {
				toast.success("Bravo!");
			}, 500);
			setTimeout(() => {
				window.location.reload();
			}, 500);
		})	
	  }
	return (
		<div>
		<Toaster position="bottom-center" reverseOrder={false} />
		<div className="pxp-dashboard-content-details" >
		<h1>Votre contrat Wselect</h1>
		<div className="row">
			<div className="col-8">
				<div className="row justify-content-between align-content-center">
					<div className="mt-3 mt-lg-4">
						<div className="pxp-single-job-side-panel" style={{ backgroundColor: '#e6f0f9'}} >			
									<div>
										<div className="table-responsive">
								<table className="table table-hover align-middle">
									<thead>
										<tr>
											<th>Contrat entreprise</th>
											<th>&nbsp;</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><div className="pxp-company-dashboard-subscriptions-plan">{documents[0]?.name}</div></td>
											<td>
												<div className="pxp-dashboard-table-options">
													<ul className="list-unstyled">
														<li><a href={`${process.env.REACT_APP_API_STATIC}/static/docs/${documents[0]?.path}`} title="Télécharger"><span className="fa fa-upload"></span></a></li>
													</ul>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div className="table-responsive">
								<table className="table table-hover align-middle">
									<thead>
										<tr>
											<th>Documents</th>
											<th>&nbsp;</th>
										</tr>
									</thead>
									<tbody>
									{documents.map((el, i) => (	
										el.id !== 1 ? (
										<tr key={`element${i}`}>
										  	<td>
												<div className="pxp-company-dashboard-subscriptions-plan">{el.name}</div>
										 	</td>
										  	<td>
												<div className="pxp-dashboard-table-options">
													<ul className="list-unstyled">
														<li>
															<a href={`${process.env.REACT_APP_API_STATIC}/static/docs/${el.path}`} title="Télécharger">
																<span className="fa fa-download"></span>
															</a>
														</li>
													</ul>
												</div>
										 	</td>
										</tr>
									 		 ) : null
										))}
									</tbody>
								</table>		
							</div>
							<div className="table-responsive">
							{
									status=== 'Devis' ?
									<table className="table table-hover align-middle">
										<thead>
										<tr>
											<th>&nbsp;</th>
											<th>&nbsp;</th>
										</tr>
									</thead>
									<tbody>
									<tr>
										  <td>
											<div className="pxp-company-dashboard-subscriptions-plan">
												
												{Contract?.name}
												<label htmlFor="link" className="mt-4">
														{
															employerContract.length === 0 ?
															<span className="badge rounded-pill bg-success" style={{marginLeft:'10px'}}>Pas encore uploadé</span>
															: 
															<span className="badge rounded-pill bg-success" style={{marginLeft:'10px'}}>En Cours de Traitement </span>

														}
												</label>	
											</div>
											
										  </td>
										  <td>
											<div className="pxp-dashboard-table-options">
											  <ul className="list-unstyled">
												{
													file === ''?
													<li>
													
														<label htmlFor="upload" style={{marginTop:'55px'}}>
														<span className="fa fa-upload m-3" style={{color:'	#0a58ca'}}></span>
															<form
															className="mt-4"
															encType="multipart/form-data"
														>
														<input type="file" id="upload" onChange={(e)=>handleFileChange(e)}  style={{display:'none'}}  />
														</form>
														</label>
													</li> 
													: null
												}
												{
													file === ''?	
													<li>
													
													</li>
													: 	
													<li >
														<button title="Enregistrer" onClick={handleUpload}><span className="fa fa-upload"></span></button>
													</li>
													}
											  </ul>
											</div>
										  </td>
										</tr>
									</tbody>
								</table>
								: null
								}
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
			<div className="col-4">
				<div className="mt-3 mt-lg-4">
					<div className="pxp-dashboard-stats-card bg-primary bg-opacity-10 mb-3 mb-xxl-0">
						<div className="pxp-dashboard-stats-card-icon text-primary">
							<span className="fa fa-file-text-o"></span>
						</div>
						<div className="pxp-dashboard-stats-card-info">
							<div className="pxp-dashboard-stats-card-info-number">{offers }</div>
							<div className="pxp-dashboard-stats-card-info-text pxp-text-light">Offres</div>
						</div>
					</div>
				</div>
				<div className="mt-3 mt-lg-4">
					<div className="pxp-dashboard-stats-card bg-success bg-opacity-10 mb-3 mb-xxl-0">
						<div className="pxp-dashboard-stats-card-icon text-success">
							<span className="fa fa-user-circle-o"></span>
						</div>
						<div className="pxp-dashboard-stats-card-info">
							<div className="pxp-dashboard-stats-card-info-number">{candidates}</div>
							<div className="pxp-dashboard-stats-card-info-text pxp-text-light">Candidatures</div>
						</div>
					</div>
				</div>
				{
					status=== 'Contract' || status=== 'Client' ? 	
					<div className="table-responsive mt-4 mt-lg-4">
					<table className="table table-hover align-middle">
						<tbody>
							<tr>
								<td><div className="pxp-company-dashboard-subscriptions-plan">Demande d'evaluation</div></td>
								<td>
									<div className="pxp-dashboard-table-options">
										<ul className="list-unstyled">
											<li><button title="Consulter" onClick={()=>{setComponent('EvaluationRequest')}} > <span className="fa fa-external-link "></span></button></li>
										</ul>
									</div>
								</td>
							</tr>
							<tr>
								<td><div className="pxp-company-dashboard-subscriptions-plan">Nouvelle offre d'emploi</div></td>
								<td>
									<div className="pxp-dashboard-table-options">
										<ul className="list-unstyled">
											<li><button title="Ajouter une nouvelle offre"  onClick={()=>{setComponent('AddOffer')}} ><span className="fa fa-external-link "></span></button></li>
										</ul>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div> : null
				}
			</div>
		</div>
	</div> 
	</div>
	)
}
export default DocumentsList;
import React, { useState, useEffect,useContext } from "react";
import {instance} from '../../lib/axios'
import EvaluationRequest from '../../pages/Dashboard/employer/EvaluationRequest'
import DocumentsList from './DocumentList'
import AddOffer from "../offers/AddOffer";
import { AuthContext } from "../../context/Auth";


function Documents() {
	const { auth } = useContext(AuthContext);
	const [candidatesCount, setCandidatesCount] = useState([]);
	const [waiting, setWaiting] = useState(true);
	const [offerCount, setOfferCount] = useState(0)
	let [component, setComponent] = useState('DocumentsList')
	useEffect(() => {
		let CandidateCount = 0 ;
		instance.get(`employers/${auth.id}`)
		.then((response)=>{
			setOfferCount(response.data.results.Offer.length)
			response.data.results.Offer.forEach(element => {
				CandidateCount+= element.canidate.length
			});
		})
		setCandidatesCount(CandidateCount)
		setWaiting(false)
	}, []);
	return (
		<>
		{
			waiting ? 	<div className="pxp-dashboard-content-details" >
			<div className="text-center">
				<div className="spinner-border" role="status">
					<span className="sr-only">Loading...</span>
				</div>
			</div>
	</div> :
			 component==='DocumentsList' ?
			 <DocumentsList candidates={candidatesCount} offers={offerCount} setComponent={setComponent} />
			 :component === 'EvaluationRequest' ? <EvaluationRequest/>
			 :  <AddOffer />
			
		}
		

			<footer>
				<div className="pxp-footer-copyright pxp-text-light">
					© {new Date().getFullYear()} World select. All Right
					Reserved.
				</div>
			</footer>
		</>
	);
}

export default Documents;

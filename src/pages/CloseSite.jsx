import React from "react";
import Bg from "../assets/images/hero-3.jpeg";
import Logo from "../assets/images/logo-ws.png";

function CloseSite() {
	return (
		<div
			className="container-cs"
			style={{
				backgroundImage: `url(${Bg})`,
			}}
		>
			<div className="header-cs">
				<img
					src={Logo}
					alt="LOGO"
					style={{
						width: "200px",
					}}
				/>
			</div>
			<div className="body-cs">
				<p>NOTRE SITE WEB EST</p>
				<h3>EN MAINTENANCE</h3>
			</div>
		</div>
	);
}

export default CloseSite;

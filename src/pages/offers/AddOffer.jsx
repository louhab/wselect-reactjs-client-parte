import React, { useState, useEffect} from "react";
import { instance } from "../../lib/axios";
import toast, { Toaster } from "react-hot-toast";
import { useForm } from 'react-hook-form';
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";

export default function AddOffer() {
    const [companies, setCompanies] = useState([]);
    const fetchCompanies = async () => {
        try {
            const { data } = await instance.get(`/employers?skip=0&take=12`);
            setCompanies(data.results.allemployers);
        } catch (error) {
            console.error("Error fetching companies:", error);
        }
    }
    useEffect(() => {
        fetchCompanies();
    }, []);
    const schema = Joi.object({
        title: Joi.string().max(200).required().label("Nom de l'offre").messages({
            "string.empty": "Nom de l'offre  est obligatoire",
        }),
        employerId: Joi.string().required().label('Employer').messages({
            "string.empty": "Entreprise  est obligatoire",
        }),
        diplomasName: Joi.string().max(200).required().label("Nom du Diplome").messages({
            "string.empty": "Nom du Diplome est obligatoire",
        }),
        description: Joi.string().max(200).required().label("description")
            .messages({
                "string.empty": "Description est obligatoire",
            }),
		isDiplomasRequired: Joi.boolean().required().label("requirement")
		.messages({
			'boolean.base': 'Obligatoin du diplome est obligatoire',
		}),
        minExpYears: Joi.string().required().label("Expérience minimale")
            .messages({
                "string.empty": "Expérience minimale est obligatoire",
            }),
		minLanguageLevel: Joi.string().required().label("Niveau du langue minimale")
            .messages({
                "string.empty": "Niveau du langue minimale est obligatoire",
            }),
    });

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm({
        resolver: joiResolver(schema),
    });

    const onSubmit = async (values) => {
        try {
            await instance.post("/offers", values);
            toast.success("Bravo!, l'offre est ajoutée");
            setTimeout(() => {
                window.location.reload(false);
            }, 500);
        } catch (error) {
            console.error("Error submitting form:", error);
        }
    };

    return (
        <>
            <div className="pxp-dashboard-content-details">
                <Toaster position="bottom-center" reverseOrder={false} />
                <h1>Ajouter une nouvelle offre</h1>
                <form onSubmit={handleSubmit(onSubmit)}>
					<div className="row mt-4 mt-lg-5">
						<div className="col-xxl-6">
							<div className="mb-3">
								<label htmlFor="pxp-company-name" className="form-label">Nom de l'offre</label>
								<input 
								style={{
									border:
										errors["title"] &&
										errors["title"].message
											? "1px solid rgba(255, 0, 0, 0.3)"
											: null, 
									  }}
								{...register('title')} type="text" id="pxp-company-name" className="form-control" placeholder="Nom de l'offre" />
								{errors && errors.title && <p className="text-danger">{errors.title.message}</p>}
							</div>
							<div className="row">
								<div className="col-sm-12">
									<div className="mb-3">
										<label htmlFor="pxp-company-size" className="form-label">Entreprises</label>
										<select
										style={{
											border:
												errors["employerId"] &&
												errors["employerId"].message
													? "1px solid rgba(255, 0, 0, 0.3)"
													: null, 
											  }}
											  {...register('employerId')} 
										id="pxp-company-size" className="form-select">
											<option value=""></option>
											{companies.map(company => (
												<option key={company.id} value={company.id}>{company.companyName}</option>
											))}
										</select>
										{errors && errors.employerId && <p className="text-danger">{errors.employerId.message}</p>}

									</div>
								</div>
							</div>
						</div>
						<div className="col-xxl-4">
							<label htmlFor="pxp-company-name" className="form-label">Nom du diplome</label>
							<input 
							style={{
								border:
									errors["diplomasName"] &&
									errors["diplomasName"].message
										? "1px solid rgba(255, 0, 0, 0.3)"
										: null, 
								  }}
							{...register('diplomasName')} type="text" id="pxp-company-name" className="form-control" placeholder="Nom du diplome" />
							{errors && errors.diplomasName && <p className="text-danger">{errors.diplomasName.message}</p>}
						</div>
						<div className="col-xxl-2">
							<label htmlFor="pxp-company-name" className="form-label">Obligatoire ?</label>
							<select
							style={{
								border:
									errors["isDiplomasRequired"] &&
									errors["isDiplomasRequired"].message
										? "1px solid rgba(255, 0, 0, 0.3)"
										: null, 
								  }}
							{...register('isDiplomasRequired')} id="pxp-company-size" className="form-select">
								<option value=""></option>
								<option value={true}>Oui</option>
								<option value={false}>Non</option>
							</select>
							{errors && errors.isDiplomasRequired && <p className="text-danger">{errors.isDiplomasRequired.message}</p>}
						</div>
					</div>
					<div className="row">
						<div className="col-xxl-6">
							<label htmlFor="pxp-company-name" className="form-label">Expérience minimale</label>
							<select
							style={{
								border:
									errors["minExpYears"] &&
									errors["minExpYears"].message
										? "1px solid rgba(255, 0, 0, 0.3)"
										: null, 
								  }}
							{...register('minExpYears')} id="pxp-company-size" className="form-select"
								
							>
								<option value=""></option>
								<option value="one">one</option>
								<option value="two">two</option>
								<option value="three">three</option>
							</select>
							{errors && errors.minExpYears && <p className="text-danger">{errors.minExpYears.message}</p>}
						</div>
						<div className="col-xxl-6">
							<label htmlFor="pxp-company-name" className="form-label">Niveau du langue minimale</label>
							<select
							style={{
								border:
									errors["minLanguageLevel"] &&
									errors["minLanguageLevel"].message
										? "1px solid rgba(255, 0, 0, 0.3)"
										: null, 
								  }}
							{...register('minLanguageLevel')} id="pxp-company-size" className="form-select"
							>
								<option value=""></option>
								<option value="one">one</option>
								<option value="two">two</option>
								<option value="three">three</option>
							</select>
							{errors && errors.minLanguageLevel && <p className="text-danger">{errors.minLanguageLevel.message}</p>}
						</div>
					</div>
					<div className="mb-3">
						<label htmlFor="pxp-company-about" className="form-label">Description de l'offre</label>
						<textarea 
						style={{
							border:
								errors["description"] &&
								errors["description"].message
									? "1px solid rgba(255, 0, 0, 0.3)"
									: null, 
							  }}
						{...register('description')} className="form-control"  id="pxp-company-about" placeholder=""></textarea>
						{errors && errors.description && <p className="text-danger">{errors.description.message}</p>}
					</div>
					<div className="mt-4 mt-lg-5">
						<button className="btn rounded-pill pxp-section-cta">Sauvegarder</button>
					</div>
                </form>
            </div>
        </>
    );
}

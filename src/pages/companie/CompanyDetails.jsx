import React, { useEffect, useState,useContext } from "react";
import { instance } from "../../lib/axios";
import SidePanel from "../../components/layout/Dashboard/SidePanel";
import NavBar from "../../components/layout/Dashboard/navBar";
import { AuthContext } from "../../context/Auth";

export default function CompanyDetails() {
	let id = 1 
	const [company, setCompany] = useState(null)
	const { auth } = useContext(AuthContext);
	const user = auth?.user;
	const [position, setPosition] = useState(0);
	const [profile, setProfile] = useState(user);
	const [show, setShow] = useState(0);
	const panel = [
		{ spanClass: "fa fa-home", title: "Dashboard", status: 1 },
		{ spanClass: "fa fa-user-circle", title: "Contrat", status: 1 },
		{ spanClass: "fa fa-user-circle-o", title: "Candidates", status: 1 },
	];


	return (
		<>
		<div
			style={{
				position: "relative",
				backgroundColor: "#e6f0f9",
				display: "flex",
				flexDirection: "row",
				width: "100%",
			}}
		>
			<SidePanel
				position={position}
				setPosition={setPosition}
				panel={panel}
				setShow={setShow}
				show={show}
			/>
			<div
				style={{
					position: "relative",
					padding: "20px 20px",
					width: "100%",
				}}
			>
				<NavBar
					setShow={setShow}
					candidate={`${profile.firstName} ${profile.lastName}`}
				/>
			
			<div className="pxp-dashboard-content">
				<div className="pxp-dashboard-content-header">
					
				</div>
			</div>
		
	
			</div>
		</div>
		</>
	);
}

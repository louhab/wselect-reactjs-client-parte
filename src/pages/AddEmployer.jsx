import React, { Suspense } from "react";
import Logo from "../assets/images/logo-ws.png";
import PreLoader from "../components/PreLoader";
import { useLocation } from "react-router-dom";

const Header = React.lazy(() => import("../components/layout/Header"));
const Footer = React.lazy(() => import("../components/layout/Footer"));
const AddEmployerFrom = React.lazy(() =>
	import("../components/forms/AddEmployerFrom")
);

export default function AddEmployer() {
	const { search } = useLocation();

	const query = React.useMemo(() => new URLSearchParams(search), [search]);

	return (
		<>
			<Suspense fallback={<PreLoader />}>
				<Header
					logo={Logo}
					header="pxp-header fixed-top pxp-no-bg pxp-has-border"
					nav="pxp-nav dropdown-hover-all d-none d-xl-block"
					extraNav="pxp-user-nav d-none d-sm-flex pxp-on-light"
				/>
				<AddEmployerFrom query={query} />
				<Footer />
			</Suspense>
		</>
	);
}

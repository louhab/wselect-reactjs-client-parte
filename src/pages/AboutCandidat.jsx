import React, { Suspense } from "react";
import AboutHero from "../components/about/AboutHero";
import Footer from "../components/layout/Footer";
import Header from "../components/layout/Header";
import Background from "../assets/images/Services-condidat.jpg";
import AboutCandidat from "../components/about/AboutCandidat";
import Pricing from "../components/Pricing";
// import Logo from "../assets/images/Logo-HR-Globe-Profil.png";
import Logo from "../assets/images/logo-ws.png";
import PreLoader from "../components/PreLoader";

export default function Candidat() {
	return (
		<>
			<Suspense fallback={<PreLoader />}>
				<Header
					logo={Logo}
					header="pxp-header fixed-top pxp-bg pxp-is-sticky"
					nav="pxp-nav dropdown-hover-all d-none d-xl-block"
					extraNav="pxp-user-nav d-none d-sm-flex"
				/>
				<AboutHero
					bg={Background}
					content={() => (
						<div className="text-center">
							<h1>
							Faites le tremplin vers l'emploi qui vous convient
							</h1>
						</div>
					)}
				/>
				
				<Pricing />
				<Footer />
			</Suspense>
		</>
	);
}

// todo
// make template layout to avoid importing components in evry new page

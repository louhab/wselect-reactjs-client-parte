/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState, useContext } from "react";
import Content from "../../components/layout/Dashboard/Content";
import SidePanel from "../../components/layout/Dashboard/SidePanel";
import CandidatePtofile from "../../pages/Dashboard/Candidateprofile";
import CandidateGeneralData from "../../pages/Dashboard/candidate/CandidateGeneralData";
import Inbox from "../../pages/inbox";
import CandidateAnnoncesList from "../../pages/Dashboard/CandidateAnnoncesList";
import Password from "../../pages/Dashboard/password";
import NavBar from "../../components/layout/Dashboard/navBar";
import { instance } from "../../lib/axios";
import { AuthContext } from "../../context/Auth";

const CondidateDetails = ({ id }) => {
	const { auth } = useContext(AuthContext);
	const user = auth?.user;
	const [position, setPosition] = useState(1);
	const [profile, setProfile] = useState(user);
	const [show, setShow] = useState(0);

	//need make login work
	// const candidateId = id;

	useEffect(() => {
		instance
			.get(`/candidates/${id}`)
			.then((res) => {
				const data = res?.data?.results;
				setProfile({
					firstName: data?.firstName,
					lastName: data?.lastName,
					email: data?.email,
					phone: data?.phone,
					gender: data?.gender,
					address: data?.address,
					diplomasName: data?.diplomasName,
					yearsExp: data?.yearsExp,
					languageLevel: data?.languageLevel,
					diplomeId: data?.diplomeId,
					domaineDactiviteId: data?.domaineDactiviteId,
				});
			})
			.catch((err) => {
				console.log(err);
			});
	}, []);

	const panelCandidate = [
		{ spanClass: "fa fa-home", title: "Dashboard", status: 1 },
		{ spanClass: "fa fa-pencil", title: "Edit Profile", status: 1 },
		{
			spanClass: "fa fa-question-circle-o",
			title: "Questionnaire",
			status: 1,
		},
		{ spanClass: "fa fa-lock", title: "Change Password", status: 1 },
		{ spanClass: "fa fa-home", title: "Annonces", status: 1 },
	];

	return (
		<div
			style={{
				position: "relative",
				backgroundColor: "#",
				display: "flex",
				flexDirection: "row",
				width: "100%",
			}}
		>
			<SidePanel
				position={position}
				setPosition={setPosition}
				panel={panelCandidate}
				setShow={setShow}
				show={show}
			/>
			<div
				style={{
					position: "relative",
					padding: "20px 20px",
					width: "100%",
				}}
			>
				<NavBar
					setShow={setShow}
					candidate={`${profile.firstName} ${profile.lastName}`}
				/>
				{position === 0 ? (
					<Content />
				) : position === 1 ? (
					<CandidatePtofile data={{ profile, id }} />
				) : position === 2 ? (
					<CandidateGeneralData data={{ profile, id }} />
				) : position === 3 ? (
					<Password data={{ id, user: "candidates" }} />
				) : position === 4 ? (
					<CandidateAnnoncesList data={{ profile, id }} />
				) : position === 20 ? (
					<Inbox />
				) : (
					<>ok</>
				)}
			</div>
		</div>
	);
};

export default CondidateDetails;

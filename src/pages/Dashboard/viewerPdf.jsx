import React, { useState } from "react";
import { Link } from "react-router-dom";

export const InputView = ({ setFile, id, cv }) => {
	const [url, setUrl] = useState("");

	const onChange = (e) => {
		const files = e.target.files;
		setFile(e.target.files[0]);
		files.length > 0 && setUrl(URL.createObjectURL(files[0]));
	};

	// if (cv)
	// 	return (
	// 		<div className="mt-4 mt-lg-5">
	// 			<Link to={`/${id}/candidate.pdf`}>Your CV</Link>
	// 		</div>
	// 	);

	return (
		<div>
			<div>
				{url ? (
					<div className="mt-4 mt-lg-5">CV</div>
				) : (
					<>
						<div className="form-label">&nbsp;</div>
						<div className="pxp-candidate-cover mb-3">
							<input
								name={id}
								type="file"
								id="pxp-candidate-cover-choose-file"
								accept="application/pdf"
								onChange={onChange}
							/>
							<label
								htmlFor="pxp-candidate-cover-choose-file"
								className="pxp-cover"
							>
								<span>Upload Your CV</span>
							</label>
						</div>
					</>
				)}
			</div>
		</div>
	);
};

import React, { useEffect, useState } from "react";
import { Input, Select } from "../../components/inputDash/index";
import { instance } from "../../lib/axios";
import { LEVELS } from "./statiqueList";
import { Eggy } from "@s-r0/eggy-js";
import { useFormik } from "formik";
import * as Yup from "yup";

const Employerprofile = ({ data }) => {
	const { profile, id } = data;
	const [domainDactivityList, setdomainDactivityList] = useState([]);
	const [display, setDisplay] = useState(false);

	useEffect(() => {
		instance
			.get("/domaines")
			.then((res) => {
				setdomainDactivityList(res?.data?.results);
			})
			.catch((err) => {
				console.log(err);
			});
	}, []);

	const validationSchema = Yup.object({});

	const formik = useFormik({
		initialValues: {},
		validationSchema,
		onSubmit: (values) => {
			setDisplay(true);
			instance
				.put(`/employers/${id}`, {
					...values,
				})
				.then(async (res) => {
					await Eggy({
						title: "Success",
						message: "offer created !",
						type: "success",
					});
					setDisplay(false);
				})
				.catch(async (err) => {
					await Eggy({
						title: "Error",
						message: "offer doesn't created !",
						type: "error",
					});
					console.log(err);
				});
		},
	});
	return (
		<div
			style={{
				justifySelf: "center",
				padding: "100px 0px",
				width: "80%",
			}}
		>
			<div className="pxp-dashboard-content-details">
				<h1>Editer le profil</h1>
				<p className="pxp-text-light">
					Modifiez les informations de votre page de profil de
					candidat.
				</p>
				<form onSubmit={formik.handleSubmit}>
					<div className="row mt-4 mt-lg-5">
						<div className="col-xxl-8">
							<div className="row">
								<Input
									formik={formik}
									title={"Nom de l'entreprise"}
									id={"firstName"}
								/>
							</div>
							<div className="row">
								<div className="col-sm-6">
									<Input
										formik={formik}
										title={"Email"}
										id={"email"}
									/>
								</div>
								<div className="col-sm-6">
									<Input
										formik={formik}
										title={"Phone"}
										id={"phone"}
									/>
								</div>
							</div>
							<Input
								formik={formik}
								title=" Website"
								id="companyWebsite"
							/>
							<div className="col-xxl-4">
								<div className="form-label">&nbsp;</div>
								<div className="pxp-company-cover mb-3">
									<input
										type="file"
										id="pxp-company-cover-choose-file"
										accept="image/*"
									/>
									<label
										htmlFor="pxp-company-cover-choose-file"
										className="pxp-cover"
									>
										<span>Upload Cover Image</span>
									</label>
								</div>
								<div className="pxp-company-logo mb-3">
									<input
										type="file"
										id="pxp-company-logo-choose-file"
										accept="image/*"
									/>
									<label
										htmlFor="pxp-company-logo-choose-file"
										className="pxp-cover"
									>
										<span>
											Upload
											<br />
											Logo
										</span>
									</label>
								</div>
							</div>

							<div className="mt-4 mt-lg-5">
								<div className="mb-3">
									<label
										htmlFor="pxp-company-about"
										className="form-label"
									>
										About the company
									</label>
									<textarea
										className="form-control"
										id="pxp-company-about"
										placeholder="Type your info here..."
									/>
								</div>
								<div className="row">
									<div className="col-md-4">
										<Select
											formik={formik}
											title={"Industrie"}
											id={"domaine"}
											list={domainDactivityList}
										/>
									</div>
									<div className="col-md-4">
										<Input
											formik={formik}
											title={"Fondé en"}
											id={"foundedIn"}
										/>
									</div>
									<div className="col-md-4">
										<Select
											formik={formik}
											title="Taille de l'entreprise"
											id=" companySize "
											list={LEVELS}
										/>
									</div>
								</div>
							</div>
							<div className="mt-4 mt-lg-5">
								<h2>Emplacement de la société</h2>
								<div className="row">
									<div className="col-sm-6">
										<Input
											formik={formik}
											title={"Pays"}
											id={"country"}
										/>
									</div>

									<div className="col-md-6">
										<Input
											formik={formik}
											title={"ville"}
											id={"city"}
										/>
									</div>
								</div>
								<Input
									formik={formik}
									title={"Adresse"}
									id={"address"}
								/>
							</div>
							<div className="mt-4 mt-lg-5">
								<h2>Des médias sociaux</h2>
								<div className="row">
									<div className="col-md-6">
										<Input
											formik={formik}
											title={"Facebook"}
											id={"address"}
										/>
									</div>
									<div className="col-md-6">
										<Input
											formik={formik}
											title={"Twitter"}
											id={"address"}
										/>
									</div>
									<div className="col-md-6">
										<Input
											formik={formik}
											title={"Instagram"}
											id={"address"}
										/>
									</div>
									<div className="col-md-6">
										<Input
											formik={formik}
											title={"Linkedin"}
											id={"address"}
										/>
									</div>
								</div>
							</div>
						</div>
					</div>
					<SubmitButton display={display} />
				</form>
			</div>
		</div>
	);
};

export default Employerprofile;

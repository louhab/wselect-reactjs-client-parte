import React, { useState, useEffect } from "react";
import { instance } from "../../../lib/axios";
import toast, { Toaster } from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import { useAutosave } from "react-autosave";

export function formatDate(date) {
	var d = new Date(date),
		month = "" + (d.getMonth() + 1),
		day = "" + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2) month = "0" + month;
	if (day.length < 2) day = "0" + day;

	return [year, month, day].join("-");
}

export default function EvaluationRequest() {
	const navigate = useNavigate();

	const [form, setForm] = useState({
		accountNumber: "",
		companyName: "",
		companyAddress: "",
		companyCity: "",
		companyProvince: "",
		companyCountry: "",
		companyZip: "",
		companyAddressBis: "",
		companyCityBis: "",
		companyProvinceBis: "",
		companyCountryBis: "",
		companyZipBis: "",
		website: "",
		startDate: "",
		isIndividual: false,
		isPartnership: false,
		isCompany: false,
		isCooperative: false,
		isNonprofit: false,
		isRegisteredCharity: false,

		// Two
		firstName: "",
		secondFirstName: "",
		lastName: "",
		positionTitle: "",
		firstPhone: "",
		firstPosition: "",
		secondPhone: "",
		secondPosition: "",
		faxNumber: "",
		email: "",
		address: "",
		city: "",
		province: "",
		country: "",
		zip: "",

		firstNameBis: "",
		secondFirstNameBis: "",
		lastNameBis: "",
		positionTitleBis: "",
		firstPhoneBis: "",
		firstPositionBis: "",
		secondPhoneBis: "",
		secondPositionBis: "",
		faxNumberBis: "",
		emailBis: "",
		addressBis: "",
		cityBis: "",
		provinceBis: "",
		countryBis: "",
		zipBis: "",

		// Three
		secThree1: "",
		secThree2: "", // bool
		secThree3: "", // bool
		secThree4: "",
		secThree5: "",
		secThree6: "",
		secThree7: "", // bool
		secThree8: "",
		secThree9: "", // bool
		secThree10: "",
		// Four
		tradeName: "",
		workspaceDescription: "",
		workspaceAddress: "",
		workspaceCity: "",
		workspaceProvince: "",
		workspaceZip: "",

		// Five
		secFive1: "",
		secFive2: "",
		secFive3: false, // bool
		secFive4: "",
		secFive5: "",
		secFive6: "", // bool
		secFive7: "",
		secFive8: "",
		secFive9: "", // bool
		secFive10: "",
		secFive11: "", // bool
		secFive12: "",
		secFive13: false, // bool
		secFive14: false, // bool
		secFive15: false, // bool
		secFive16: false, // bool
		secFive17: false, // bool
		secFive18: "",
		secFive19: "",
	});

	const [file, setFile] = useState();

	const saveFile = (e) => {
		setFile(e.target.files[0]);
	};

	function handleChange({ target: { name, value } }) {
		setForm((init) => ({ ...init, [name]: value }));
	}

	function handleCheck({ target: { name, checked } }) {
		setForm((init) => ({
			...init,
			[name]: checked,
		}));
	}
	function handleRadio({ target: { name, value } }) {
		setForm((init) => ({
			...init,
			[name]: value === "oui" ? true : false,
		}));
	}

	async function handleSubmit(e) {
		try {
			e.preventDefault();

			delete form.id;
			delete form.uuid;
			delete form.email;
			delete form.email;
			delete form.createdAt;
			delete form.updatedAt;

			const formData = new FormData();
			if (file) formData.append("secFive12", file);

			Object.keys(form).forEach((key) => {
				if (form[key] === "") {
					form[key] = null;
				}
			});

			for (const key in form) {
				formData.append(key, form[key]);
			}

			// formData.append(
			// 	"data",
			// 	JSON.stringify({ ...form, secFive12: file })
			// );

			await instance.put(`/employer-forms`, form);

			setTimeout(() => {
				toast.success("Bravo!");
			}, 500);
		} catch (e) {
			toast.error("Merci de remplir le formulaire", {
				style: {
					fontSize: "14px",
					borderRadius: "10px",
					background: "#333",
					color: "#fff",
				},
			});
		}
	}

	const updateForm = React.useCallback(async ({ cbData, cbFile }) => {
		delete cbData.id;
		delete cbData.uuid;
		delete cbData.email;
		delete cbData.email;
		delete cbData.createdAt;
		delete cbData.updatedAt;

		const formData = new FormData();
		if (cbFile) formData.append("secFive12", cbFile);

		Object.keys(cbData).forEach((key) => {
			if (cbData[key] === "") {
				cbData[key] = null;
			}
		});

		for (const key in cbData) {
			formData.append(key, cbData[key]);
		}
		try {
			await instance.put(`/employer-forms`, cbData);
			setTimeout(() => {
				toast.success("Auto Saved!");
			}, 500);
		} catch (error) {
			toast.error("mmmm!", {
				style: {
					fontSize: "14px",
					borderRadius: "10px",
					background: "#333",
					color: "#fff",
				},
			});
		}
	}, []);

	useEffect(() => {
		const fetchForm = async () => {
			try {
				const {
					data: { results },
				} = await instance.get(`/employer-forms`);

				const startDate = results.startDate
					? formatDate(results.startDate)
					: "";

				// delete all null properties
				Object.keys(results).forEach((key) => {
					if (results[key] === null) {
						delete results[key];
					}
				});

				setForm((init) => ({
					...init,
					...results,
					startDate,
				}));
			} catch ({
				response: {
					data: { error },
				},
			}) {
				toast.error(error?.message, {
					style: {
						fontSize: "12px",
						borderRadius: "10px",
						background: "#333",
						color: "#fff",
					},
				});
				// TODO activate this one
				// navigate("/", { replace: true });
			}
		};
		fetchForm();
	}, [navigate]);

	useAutosave({
		data: {
			cbData: form,
			cbFile: file,
		},
		onSave: updateForm,
		interval: 15000,
		saveOnUnmount: true,
	});

	return (
		<div className="pxp-dashboard-content-details"  >
			<Toaster position="bottom-center" reverseOrder={false} />
			<h1>Demande d’évaluation de l'impact sur le marché du travail.</h1>
			<form
				className="mt-4"
				onSubmit={handleSubmit}
				encType="multipart/form-data"
			>
				<br />
				{/* Section one */}
				<h3 style={{ fontWeight: "bold" }}>
					Section 1 : Renseignements sur l’entreprise de l’employeur
				</h3>
				<br />
				<div className="mb-3">
					<label htmlFor="accountNumber" className="form-label">
						1. Numéro de compte du programme des retenues sur la
						paie :
					</label>
					<input
						name="accountNumber"
						value={form?.accountNumber}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="accountNumber"
					/>
				</div>
				<div className="mb-3">
					<label htmlFor="companyName" className="form-label">
						2. Nom légal de l’entreprise :
					</label>
					<input
						name="companyName"
						value={form?.companyName}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="companyName"
					/>
				</div>
				<div className="mb-3">
					<label htmlFor="companyAddress" className="form-label">
						3. Adresse de l’entreprise :
					</label>
					<input
						name="companyAddress"
						value={form?.companyAddress}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="companyAddress"
					/>
				</div>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="companyCity" className="form-label">
								4. Ville :
							</label>
							<input
								name="companyCity"
								value={form?.companyCity}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="companyCity"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label
								htmlFor="companyProvince"
								className="form-label"
							>
								5. Province/Territoire/État :
							</label>
							<input
								name="companyProvince"
								value={form?.companyProvince}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="companyProvince"
							/>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label
								htmlFor="companyCountry"
								className="form-label"
							>
								6. Pays :
							</label>
							<input
								name="companyCountry"
								value={form?.companyCountry}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="companyCountry"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="companyZip" className="form-label">
								7. Code postal/Zip :
							</label>
							<input
								name="companyZip"
								value={form?.companyZip}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="companyZip"
							/>
						</div>
					</div>
				</div>
				<div className="mb-3">
					<label htmlFor="companyAddressBis" className="form-label">
						8. Adresse postale de l’entreprise (si différente du
						point N° 3) :
					</label>
					<input
						name="companyAddressBis"
						value={form?.companyAddressBis}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="companyAddressBis"
					/>
				</div>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label
								htmlFor="companyCityBis"
								className="form-label"
							>
								9. Ville :
							</label>
							<input
								name="companyCityBis"
								value={form?.companyCityBis}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="companyCityBis"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label
								htmlFor="companyProvinceBis"
								className="form-label"
							>
								10. Province/Territoire/État :
							</label>
							<input
								name="companyProvinceBis"
								value={form?.companyProvinceBis}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="companyProvinceBis"
							/>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label
								htmlFor="companyCountryBis"
								className="form-label"
							>
								11. Pays :
							</label>
							<input
								name="companyCountryBis"
								value={form?.companyCountryBis}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="companyCountryBis"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label
								htmlFor="companyZipBis"
								className="form-label"
							>
								12. Code postal/Zip :
							</label>
							<input
								name="companyZipBis"
								value={form?.companyZipBis}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="companyZipBis"
							/>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="website" className="form-label">
								13. Site Internet :
							</label>
							<input
								name="website"
								value={form?.website}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="website"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="startDate" className="form-label">
								14. Date du début de l’entreprise :
							</label>
							<input
								name="startDate"
								value={form?.startDate}
								onChange={handleChange}
								type="date"
								className="form-control"
								id="startDate"
							/>
						</div>
					</div>
				</div>
				<div className="mb-3">
					<label htmlFor="last-name" className="form-label">
						15. Structure et type d’organisation (cochez toutes les
						cases qui s’appliquent)
					</label>
					<br />
					<label
						className="form-label"
						style={{ marginRight: "10px" }}
					>
						Entreprise :
					</label>
					<div className="form-check form-check-inline">
						<input
							name="isIndividual"
							className="form-check-input"
							type="checkbox"
							id="isIndividual"
							onChange={handleCheck}
							checked={form.isIndividual}
						/>
						<label
							className="form-check-label"
							htmlFor="isIndividual"
						>
							Individuelle
						</label>
					</div>
					<div className="form-check form-check-inline">
						<input
							name="isPartnership"
							className="form-check-input"
							type="checkbox"
							id="isPartnership"
							onChange={handleCheck}
							checked={form.isPartnership}
						/>
						<label
							className="form-check-label"
							htmlFor="isPartnership"
						>
							Partenariat
						</label>
					</div>
					<div className="form-check form-check-inline">
						<input
							name="isCompany"
							className="form-check-input"
							type="checkbox"
							id="isCompany"
							onChange={handleCheck}
						/>
						<label className="form-check-label" htmlFor="isCompany">
							Société
						</label>
					</div>
					<div className="form-check form-check-inline">
						<input
							name="isCooperative"
							className="form-check-input"
							type="checkbox"
							id="isCooperative"
							onChange={handleCheck}
							checked={form.isCooperative}
						/>
						<label
							className="form-check-label"
							htmlFor="isCooperative"
						>
							Coopérative
						</label>
					</div>
					<br />
					<label
						className="form-label"
						style={{ marginRight: "10px" }}
					>
						Autre :
					</label>

					<div className="form-check form-check-inline">
						<input
							name="isNonprofit"
							className="form-check-input"
							type="checkbox"
							id="isNonprofit"
							onChange={handleCheck}
							checked={form.isNonprofit}
						/>
						<label
							className="form-check-label"
							htmlFor="isNonprofit"
						>
							Sans but lucratif
						</label>
					</div>

					<div className="form-check form-check-inline">
						<input
							name="isRegisteredCharity"
							className="form-check-input"
							type="checkbox"
							id="isRegisteredCharity"
							onChange={handleCheck}
							checked={form.isRegisteredCharity}
						/>
						<label
							className="form-check-label"
							htmlFor="isRegisteredCharity"
						>
							Organisme de bienfaisance enregistré
						</label>
					</div>
				</div>
				<br />
				<h3 style={{ fontWeight: "bold" }}>
					Section 2 : Renseignements sur l’employeur
				</h3>
				<h6
					style={{
						fontWeight: "100",
						fontSize: "12px",
						fontStyle: "italic",
						lineHeight: "normal",
						marginTop: "6px",
					}}
				>
					Personne - ressource principale de l’employeur (Cette
					personne doit être l'employeur ou un employé de l'employeur)
				</h6>
				<br />
				<div className="row">
					<div className="col-sm-4">
						<div className="mb-3">
							<label htmlFor="firstName" className="form-label">
								1. Prénom :
							</label>
							<input
								name="firstName"
								value={form?.firstName}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="firstName"
							/>
						</div>
					</div>
					<div className="col-sm-4">
						<div className="mb-3">
							<label
								htmlFor="secondFirstName"
								className="form-label"
							>
								Deuxième prénom :
							</label>
							<input
								name="secondFirstName"
								value={form?.secondFirstName}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="secondFirstName"
							/>
						</div>
					</div>
					<div className="col-sm-4">
						<div className="mb-3">
							<label htmlFor="lastName" className="form-label">
								Nom de famille :
							</label>
							<input
								name="lastName"
								value={form?.lastName}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="lastName"
							/>
						</div>
					</div>
				</div>
				<div className="mb-3">
					<label htmlFor="positionTitle" className="form-label">
						2. Titre du poste :
					</label>
					<input
						name="positionTitle"
						value={form?.positionTitle}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="positionTitle"
					/>
				</div>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="firstPhone" className="form-label">
								3. Numéro de téléphone :
							</label>
							<input
								name="firstPhone"
								value={form?.firstPhone}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="firstPhone"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label
								htmlFor="firstPosition"
								className="form-label"
							>
								Poste :
							</label>
							<input
								name="firstPosition"
								value={form?.firstPosition}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="firstPosition"
							/>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="secondPhone" className="form-label">
								4. Autre numéro de téléphone :
							</label>
							<input
								name="secondPhone"
								value={form?.secondPhone}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="secondPhone"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label
								htmlFor="secondPosition"
								className="form-label"
							>
								Poste :
							</label>
							<input
								name="secondPosition"
								value={form?.secondPosition}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="secondPosition"
							/>
						</div>
					</div>
				</div>
				<div className="mb-3">
					<label htmlFor="faxNumber" className="form-label">
						5. Numéro du télécopieur :
					</label>
					<input
						name="faxNumber"
						value={form?.faxNumber}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="faxNumber"
					/>
				</div>
				<div className="mb-3">
					<label htmlFor="email" className="form-label">
						6. Adresse courriel :
					</label>
					<input
						name="email"
						value={form?.email}
						// onChange={handleChange}
						type="text"
						className="form-control"
						id="email"
						disabled
					/>
				</div>
				<div className="mb-3">
					<label htmlFor="address" className="form-label">
						7. Adresse postale :
					</label>
					<input
						name="address"
						value={form?.address}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="address"
					/>
				</div>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="city" className="form-label">
								8. Ville :
							</label>
							<input
								name="city"
								value={form?.city}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="city"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="province" className="form-label">
								9. Province/Territoire/État :
							</label>
							<input
								name="province"
								value={form?.province}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="province"
							/>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="country" className="form-label">
								10. Pays :
							</label>
							<input
								name="country"
								value={form?.country}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="country"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="zip" className="form-label">
								11. Code postal/Zip :
							</label>
							<input
								name="zip"
								value={form?.zip}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="zip"
							/>
						</div>
					</div>
				</div>
				<br />
				<h6
					style={{
						fontWeight: "100",
						fontSize: "12px",
						fontStyle: "italic",
						lineHeight: "normal",
						marginTop: "6px",
					}}
				>
					Autre Personne - ressource principale de l’employeur (Cette
					personne doit être l'employeur ou un employé de l'employeur)
				</h6>
				{/* section 2 bis */}
				<div className="row">
					<div className="col-sm-4">
						<div className="mb-3">
							<label
								htmlFor="firstNameBis"
								className="form-label"
							>
								12. Prénom :
							</label>
							<input
								name="firstNameBis"
								value={form?.firstNameBis}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="firstNameBis"
							/>
						</div>
					</div>
					<div className="col-sm-4">
						<div className="mb-3">
							<label
								htmlFor="secondFirstNameBis"
								className="form-label"
							>
								Deuxième prénom :
							</label>
							<input
								name="secondFirstNameBis"
								value={form?.secondFirstNameBis}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="secondFirstNameBis"
							/>
						</div>
					</div>
					<div className="col-sm-4">
						<div className="mb-3">
							<label htmlFor="lastNameBis" className="form-label">
								Nom de famille :
							</label>
							<input
								name="lastNameBis"
								value={form?.lastNameBis}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="lastNameBis"
							/>
						</div>
					</div>
				</div>
				<div className="mb-3">
					<label htmlFor="positionTitleBis" className="form-label">
						13. Titre du poste :
					</label>
					<input
						name="positionTitleBis"
						value={form?.positionTitleBis}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="positionTitleBis"
					/>
				</div>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label
								htmlFor="firstPhoneBis"
								className="form-label"
							>
								14. Numéro de téléphone :
							</label>
							<input
								name="firstPhoneBis"
								value={form?.firstPhoneBis}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="firstPhoneBis"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label
								htmlFor="firstPositionBis"
								className="form-label"
							>
								Poste :
							</label>
							<input
								name="firstPositionBis"
								value={form?.firstPositionBis}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="firstPositionBis"
							/>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label
								htmlFor="secondPhoneBis"
								className="form-label"
							>
								15. Autre numéro de téléphone :
							</label>
							<input
								name="secondPhoneBis"
								value={form?.secondPhoneBis}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="secondPhoneBis"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label
								htmlFor="secondPositionBis"
								className="form-label"
							>
								Poste :
							</label>
							<input
								name="secondPositionBis"
								value={form?.secondPositionBis}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="secondPositionBis"
							/>
						</div>
					</div>
				</div>
				<div className="mb-3">
					<label htmlFor="faxNumberBis" className="form-label">
						16. Numéro du télécopieur :
					</label>
					<input
						name="faxNumberBis"
						value={form?.faxNumberBis}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="faxNumberBis"
					/>
				</div>
				<div className="mb-3">
					<label htmlFor="emailBis" className="form-label">
						17. Adresse courriel :
					</label>
					<input
						name="emailBis"
						value={form?.emailBis}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="emailBis"
					/>
				</div>
				<div className="mb-3">
					<label htmlFor="addressBis" className="form-label">
						18. Adresse postale :
					</label>
					<input
						name="addressBis"
						value={form?.addressBis}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="addressBis"
					/>
				</div>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="cityBis" className="form-label">
								19. Ville :
							</label>
							<input
								name="cityBis"
								value={form?.cityBis}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="cityBis"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="provinceBis" className="form-label">
								20. Province/Territoire/État :
							</label>
							<input
								name="provinceBis"
								value={form?.provinceBis}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="provinceBis"
							/>
						</div>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="countryBis" className="form-label">
								21. Pays :
							</label>
							<input
								name="countryBis"
								value={form?.countryBis}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="countryBis"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="last-name" className="form-label">
								22. Code postal/Zip :
							</label>
							<input
								name="zipBis"
								value={form?.zipBis}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="zipBis"
							/>
						</div>
					</div>
				</div>
				<br />
				<h3 style={{ fontWeight: "bold" }}>
					Section 3 : Conséquences sur le marché du travail canadien
				</h3>
				<div className="mb-3">
					<label htmlFor="secThree1" className="form-label">
						1. Combien d’employés sont actuellement embauchés à
						l’échelle nationale sous le numéro d’entreprise à 9
						chiffres de l’ARC de l’employeur ?
					</label>
					<input
						name="secThree1"
						value={form?.secThree1}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="secThree1"
					/>
				</div>
				<div className="mb-3">
					<label className="form-label">
						2. L'entreprise a-t-elle déclaré plus de 5 millions de
						dollars ($CA) en revenu brut annuel à l'agence du revenu
						du Canada (ARC) au cours de la dernière année
						d'imposition?
					</label>
					<br />

					<div className="form-check form-check-inline">
						<input
							name="secThree2"
							className="form-check-input"
							type="radio"
							id="secThree2n"
							value="non"
							onChange={handleRadio}
							checked={form.secThree2 === false}
						/>
						<label
							className="form-check-label"
							htmlFor="secThree2n"
						>
							Non
						</label>
					</div>

					<div className="form-check form-check-inline">
						<input
							name="secThree2"
							className="form-check-input"
							type="radio"
							id="secThree2o"
							value="oui"
							onChange={handleRadio}
							checked={form.secThree2 === true}
						/>
						<label
							className="form-check-label"
							htmlFor="secThree2o"
						>
							Oui
						</label>
					</div>
				</div>
				<div className="mb-3">
					<label className="form-label">
						3. Au cours des 12 derniers mois, l'employeur a-t-il mis
						à pied des employés qui occupaient le(s) poste(s)
						demandé(s) dans la présente demande?
					</label>
					<br />

					<div className="form-check form-check-inline">
						<input
							name="secThree3"
							className="form-check-input"
							type="radio"
							id="secThree3n"
							value="non"
							onChange={handleRadio}
							checked={form.secThree3 === false}
						/>
						<label
							className="form-check-label"
							htmlFor="secThree3n"
						>
							Non
						</label>
					</div>

					<div className="form-check form-check-inline">
						<input
							name="secThree3"
							className="form-check-input"
							type="radio"
							id="secThree3o"
							value="oui"
							onChange={handleRadio}
							checked={form.secThree3 === true}
						/>
						<label
							className="form-check-label"
							htmlFor="secThree3o"
						>
							Oui
						</label>
					</div>
				</div>
				<div className="mb-3">
					<label htmlFor="secThree4" className="form-label">
						4. Si vous avez répondu “Oui”, combien de
						Canadiens/résidents permanents?
					</label>
					<input
						name="secThree4"
						value={form?.secThree4}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="secThree4"
					/>
				</div>
				<div className="mb-3">
					<label htmlFor="secThree5" className="form-label">
						Combien de TET?
					</label>
					<input
						name="secThree5"
						value={form?.secThree5}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="secThree5"
					/>
				</div>
				<div className="mb-3">
					<label htmlFor="secThree6" className="form-label">
						Donnez la ou les raisons de la ou des mises à pieds :
					</label>
					<input
						name="secThree6"
						value={form?.secThree6}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="secThree6"
					/>
				</div>
				<div className="mb-3">
					<label htmlFor="last-name" className="form-label">
						5. Est-ce que l’entreprise reçoit le soutien du
						Programme de travail partagé d’Emploi et Développement
						social Canada/Service Canada?
					</label>
					<br />

					<div className="form-check form-check-inline">
						<input
							name="secThree7"
							className="form-check-input"
							type="radio"
							id="secThree7n"
							value="non"
							onChange={handleRadio}
							checked={form.secThree7 === false}
						/>
						<label
							className="form-check-label"
							htmlFor="secThree7n"
						>
							Non
						</label>
					</div>

					<div className="form-check form-check-inline">
						<input
							name="secThree7"
							className="form-check-input"
							type="radio"
							id="secThree7o"
							value="oui"
							onChange={handleRadio}
							checked={form.secThree7 === true}
						/>
						<label
							className="form-check-label"
							htmlFor="secThree7o"
						>
							Oui
						</label>
					</div>
				</div>
				<div className="mb-3">
					<label htmlFor="secThree8" className="form-label">
						6. Si vous avez répondu “Oui”, veuillez fournir des
						détails :
					</label>
					<textarea
						name="secThree8"
						value={form?.secThree8}
						onChange={handleChange}
						className="form-control"
						id="secThree8"
					/>
				</div>
				<div className="mb-3">
					<label htmlFor="last-name" className="form-label">
						7. Y a-t-il un conflit de travail en cours à l'un des
						lieux de travail visé par l'offre d'emploi?
					</label>
					<br />

					<div className="form-check form-check-inline">
						<input
							name="secThree9"
							className="form-check-input"
							type="radio"
							id="secThree9n"
							value="non"
							onChange={handleRadio}
							checked={form.secThree9 === false}
						/>
						<label
							className="form-check-label"
							htmlFor="secThree9n"
						>
							Non
						</label>
					</div>

					<div className="form-check form-check-inline">
						<input
							name="secThree9"
							className="form-check-input"
							type="radio"
							id="secThree9o"
							value="oui"
							onChange={handleRadio}
							checked={form.secThree9 === true}
						/>
						<label
							className="form-check-label"
							htmlFor="secThree9o"
						>
							Oui
						</label>
					</div>
				</div>
				<div className="mb-3">
					<label htmlFor="secThree10" className="form-label">
						8. Si vous avez répondu “Oui”, veuillez fournir des
						détails :
					</label>
					<textarea
						name="secThree10"
						value={form?.secThree10}
						onChange={handleChange}
						className="form-control"
						id="secThree10"
					/>
				</div>
				<br />
				<h3 style={{ fontWeight: "bold" }}>
					Section 4 : Lieu du travail
				</h3>
				<div className="mb-3">
					<label htmlFor="tradeName" className="form-label">
						1. Quel est le nom commercial de l'entreprise du lieu de
						travail principal?
					</label>
					<input
						name="tradeName"
						value={form?.tradeName}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="tradeName"
					/>
				</div>
				<div className="mb-3">
					<label
						htmlFor="workspaceDescription"
						className="form-label"
					>
						2. Décrire en vos mots et avec le plus de détails, la
						principale activité commerciale de ce lieu de travail :
					</label>
					<textarea
						name="workspaceDescription"
						value={form?.workspaceDescription}
						onChange={handleChange}
						className="form-control"
						id="workspaceDescription"
					/>
				</div>
				<div className="mb-3">
					<label htmlFor="workspaceAddress" className="form-label">
						3. Adresse du lieu de travail principal où le TET
						travaillera :
					</label>
					<input
						name="workspaceAddress"
						value={form?.workspaceAddress}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="workspaceAddress"
					/>
				</div>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label
								htmlFor="workspaceCity"
								className="form-label"
							>
								4. Ville :
							</label>
							<input
								name="workspaceCity"
								value={form?.workspaceCity}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="workspaceCity"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label
								htmlFor="workspaceProvince"
								className="form-label"
							>
								5. Province/Territoire/État :
							</label>
							<input
								name="workspaceProvince"
								value={form?.workspaceProvince}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="workspaceProvince"
							/>
						</div>
					</div>
				</div>
				<div className="mb-3">
					<label htmlFor="workspaceZip" className="form-label">
						6. Code postal :
					</label>
					<input
						name="workspaceZip"
						value={form?.workspaceZip}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="workspaceZip"
					/>
				</div>
				<br />
				<h3 style={{ fontWeight: "bold" }}>
					Section 5 : Heures de travail, rémunération et avantages
					sociaux
				</h3>
				<label className="form-label">
					1. Quelle est l'échelle salariale de tous les employés
					occupant actuellement le poste visé, ayant les mêmes
					compétences et années d'expérience, sur le lieu de travail
					indiqué sur ce formulaire?
				</label>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="secFive1" className="form-label">
								Le plus bas salaire $/ heure
							</label>
							<input
								name="secFive1"
								value={form?.secFive1}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="secFive1"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="secFive2" className="form-label">
								Le plus haut salaire $/ heure
							</label>
							<input
								name="secFive2"
								value={form?.secFive2}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="secFive2"
							/>
						</div>
					</div>
				</div>

				<div className="form-check form-check-inline">
					<input
						className="form-check-input"
						type="checkbox"
						name="secFive3"
						id="secFive3"
						onChange={handleCheck}
						checked={form.secFive3}
					/>
					<label className="form-check-label" htmlFor="secFive3">
						Oui, Il n’y a actuellement aucun employé occupant ce
						poste avec les mêmes compétences et années d’expérience,
						sur le lieu de travail indiqué sur ce formulaire
					</label>
				</div>

				<h6
					style={{
						fontWeight: "100",
						fontSize: "12px",
						fontStyle: "italic",
						lineHeight: "normal",
						marginTop: "6px",
					}}
				>
					Remarque: L’échelle salariale devrait être tirée des deux
					dernières périodes de paie qui ont eu lieu au cours des six
					semaines précédant la présentation de la demande.
				</h6>
				<br />

				<div className="mb-3">
					<label htmlFor="secFive4" className="form-label">
						2. Combien d’heures le TET travaillera-t-il par jour?
					</label>
					<input
						name="secFive4"
						value={form?.secFive4}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="secFive4"
					/>
				</div>

				<div className="mb-3">
					<label htmlFor="secFive5" className="form-label">
						3. Combien d’heures le TET travaillera-t-il par semaine?
					</label>
					<input
						name="secFive5"
						value={form?.secFive5}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="secFive5"
					/>
				</div>

				<div className="mb-3">
					<label className="form-label">
						4. Le TET aura-t-il un horaire atypique avec heures
						quotidiennes ou hebdomadaires variables?
					</label>
					<br />

					<div className="form-check form-check-inline">
						<input
							name="secFive6"
							className="form-check-input"
							type="radio"
							id="secFive6n"
							value="non"
							onChange={handleRadio}
							checked={form.secFive6 === false}
						/>
						<label className="form-check-label" htmlFor="secFive6n">
							Non
						</label>
					</div>

					<div className="form-check form-check-inline">
						<input
							name="secFive6"
							className="form-check-input"
							type="radio"
							id="secFive6o"
							value="oui"
							onChange={handleRadio}
							checked={form.secFive6 === true}
						/>
						<label className="form-check-label" htmlFor="secFive6o">
							Oui
						</label>
					</div>
				</div>

				<div className="mb-3">
					<label htmlFor="secFive7" className="form-label">
						5. Si vous avez répondu “Oui”, veuillez fournir des
						détails :
					</label>
					<textarea
						name="secFive7"
						value={form?.secFive7}
						onChange={handleChange}
						className="form-control"
						id="secFive7"
					/>
				</div>

				<div className="mb-3">
					<label htmlFor="secFive8" className="form-label">
						6. Quel est le salaire de base (excluant les heures
						supplémentaires) offert au TET en dollars canadiens?
					</label>
					<input
						name="secFive8"
						value={form?.secFive8}
						onChange={handleChange}
						type="text"
						className="form-control"
						id="secFive8"
					/>
				</div>
				<div className="mb-3">
					<label className="form-label">
						7. Le TET recevra-t-il un salaire éventuel (p. ex.
						travail à la pièce, kilométrage, commission, primes
						garanties, heures supplémentaires (prévisibles)?
					</label>
					<br />

					<div className="form-check form-check-inline">
						<input
							name="secFive9"
							className="form-check-input"
							type="radio"
							id="secFive9n"
							value="non"
							onChange={handleRadio}
							checked={form.secFive9 === false}
						/>
						<label className="form-check-label" htmlFor="secFive9n">
							Non
						</label>
					</div>

					<div className="form-check form-check-inline">
						<input
							name="secFive9"
							className="form-check-input"
							type="radio"
							id="secFive9o"
							value="oui"
							onChange={handleRadio}
							checked={form.secFive9 === true}
						/>
						<label className="form-check-label" htmlFor="secFive9o">
							Oui
						</label>
					</div>
				</div>
				<div className="mb-3">
					<label htmlFor="secFive10" className="form-label">
						8. Si vous avez répondu “Oui”, veuillez fournir des
						détails :
					</label>
					<textarea
						name="secFive10"
						value={form?.secFive10}
						onChange={handleChange}
						className="form-control"
						id="secFive10"
					/>
				</div>

				<div className="mb-3">
					<label className="form-label">
						9. Le poste est-il syndiqué?
					</label>
					<br />

					<div className="form-check form-check-inline">
						<input
							name="secFive11"
							className="form-check-input"
							type="radio"
							id="secFive11n"
							value="non"
							onChange={handleRadio}
							checked={form.secFive11 === false}
						/>
						<label
							className="form-check-label"
							htmlFor="secFive11n"
						>
							Non
						</label>
					</div>

					<div className="form-check form-check-inline">
						<input
							name="secFive11"
							className="form-check-input"
							type="radio"
							id="secFive11o"
							value="oui"
							onChange={handleRadio}
							checked={form.secFive11 === true}
						/>
						<label
							className="form-check-label"
							htmlFor="secFive11o"
						>
							Oui
						</label>
					</div>
				</div>

				{/* TODO input file */}

				<div className="mb-3">
					<label htmlFor="secFive12" className="form-label">
						Si vous avez répondu “Oui”, annexez la ou les sections
						de la convention collective qui énumèrent les taux de
						rémunération.
					</label>
					<input
						id="secFive12"
						type="file"
						onChange={saveFile}
						name="secFive12"
					/>
				</div>

				<div className="mb-3">
					<label className="form-label">
						10. Avantages sociaux (avantages sociaux supplémentaires
						au-delà des exigences provinciales/territoriales) :
					</label>
					<br />

					<div className="form-check form-check-inline">
						<input
							className="form-check-input"
							type="checkbox"
							id="secFive13"
							name="secFive13"
							onChange={handleCheck}
							checked={form.secFive13}
						/>
						<label className="form-check-label" htmlFor="secFive13">
							Assurance invalidité
						</label>
					</div>

					<div className="form-check form-check-inline">
						<input
							className="form-check-input"
							type="checkbox"
							id="secFive14"
							name="secFive14"
							onChange={handleCheck}
							checked={form.secFive14}
						/>
						<label className="form-check-label" htmlFor="secFive14">
							Assurance dentaire
						</label>
					</div>
					<div className="form-check form-check-inline">
						<input
							className="form-check-input"
							type="checkbox"
							id="secFive15"
							name="secFive15"
							onChange={handleCheck}
							checked={form.secFive15}
						/>
						<label className="form-check-label" htmlFor="secFive15">
							Autre avantages sociaux (veuillez préciser)
						</label>
					</div>
					<div className="form-check form-check-inline">
						<input
							className="form-check-input"
							type="checkbox"
							id="secFive16"
							name="secFive16"
							onChange={handleCheck}
							checked={form.secFive16}
						/>
						<label className="form-check-label" htmlFor="secFive16">
							Régime de retraite fourni par l’employeur
						</label>
					</div>
					<div className="form-check form-check-inline">
						<input
							className="form-check-input"
							type="checkbox"
							id="secFive17"
							name="secFive17"
							onChange={handleCheck}
							checked={form.secFive17}
						/>
						<label className="form-check-label" htmlFor="secFive17">
							Assurance-maladie complémentaire (p.ex. médicaments
							sur ordonnance, services paramédicaux, services et
							équipement médicaux)
						</label>
					</div>
				</div>

				<label className="form-label">
					11. Vacances (doit rencontrer les exigences
					provincial/territorial) :
				</label>
				<div className="row">
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="secFive18" className="form-label">
								Jours (nombre de jours ouvrables par année)
							</label>
							<input
								name="secFive18"
								value={form?.secFive18}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="secFive18"
							/>
						</div>
					</div>
					<div className="col-sm-6">
						<div className="mb-3">
							<label htmlFor="secFive19" className="form-label">
								Rémunération (% du salaire brut) S/O
							</label>
							<input
								name="secFive19"
								value={form?.secFive19}
								onChange={handleChange}
								type="text"
								className="form-control"
								id="secFive19"
							/>
						</div>
					</div>
				</div>

				<br />
				<button
					className="btn rounded-pill pxp-section-cta d-block"
					type="submit"
					style={{ width: "100%" }}
				>
					Sauvegarder
				</button>
			</form>
		</div>
	);
}

import React, { useEffect, useState } from "react";
import { Select, TextInput, Textarea } from "../../components/inputDash/index";
import { instance } from "../../lib/axios";
import { Eggy } from "@s-r0/eggy-js";
import { useForm } from "react-hook-form";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";

export const CreatOffer = ({ data }) => {
	const { id, companieId } = data;

	const [carrerLevel, setCarrerLevel] = useState([]);

	const [employmentType, setEmploymentType] = useState([]);
	const [salaryRange, setSalaryRange] = useState([]);
	const [domaines, setDomaineDactivite] = useState([]);
	const [country, setCountries] = useState([]);
	const [city, setCity] = useState([]);

	const years = [
		{ name: "1", id: "one" },
		{ name: "2", id: "two" },
		{ name: "3", id: "three" },
		{ name: "4", id: "four" },
		{ name: "5", id: "five" },
		{ name: "6", id: "six" },
		{ name: "7", id: "seven" },
		{ name: "8", id: "eight" },
		{ name: "9", id: "nine" },
		{ name: "10", id: "ten" },
	];

	const schema = Joi.object({
		name: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		description: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),

		yearsExp: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),

		carrerLevelId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
		employmentTypeId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
		salaryRangeId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
		domaineDactiviteId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
		companieId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
		employerId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
		countryId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
		cityId: Joi.number().allow(null, "").messages({
			"number.base": "Ce champ est obligatoire",
		}),
	});

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm({
		resolver: joiResolver(schema),
	});

	const onSubmit = (values) => {
		instance
			.post(`/annonces`, {
				...values,
				carrerLevelId: Number(values?.carrerLevelId),
				employmentTypeId: Number(id),
				salaryRangeId: Number(values?.salaryRangeId),
				yearsExp: values?.yearsExp,
				domaineDactiviteId: Number(values?.domaineDactiviteId),
				companieId: Number(companieId),
				employerId: Number(values?.employerId),
				countryId: Number(values?.countryId),
				cityId: Number(values?.cityId),
			})
			.then(async (res) => {
				await Eggy({
					title: "Success",
					message: "offer created !",
					type: "success",
				});
			})
			.catch(async (err) => {
				await Eggy({
					title: "Error",
					message: "offer doesn't created !",
					type: "error",
				});
				console.log(err);
			});
	};

	useEffect(() => {
		instance
			.get(`/carrer-levels`)
			.then((res) => {
				const data = res?.data?.results;
				setCarrerLevel(data);
			})
			.catch((err) => {
				console.log(err);
			});
		instance
			.get(`/employer-types`)
			.then((res) => {
				const data = res?.data?.results;
				setEmploymentType(data);
			})
			.catch((err) => {
				console.log(err);
			});
		instance
			.get(`/salary-ranges`)
			.then((res) => {
				const data = res?.data?.results;
				setSalaryRange(data);
			})
			.catch((err) => {
				console.log(err);
			});
		instance
			.get(`/domaines`)
			.then((res) => {
				const data = res?.data?.results;
				setDomaineDactivite(data);
			})
			.catch((err) => {
				console.log(err);
			});
		instance
			.get(`/countries`)
			.then((res) => {
				const data = res?.data?.results;
				setCountries(data);
			})
			.catch((err) => {
				console.log(err);
			});
		instance
			.get(`/cites`)
			.then((res) => {
				const data = res?.data?.results;
				setCity(data);
			})
			.catch((err) => {
				console.log(err);
			});
	}, []);

	return (
		<div className="pxp-dashboard-content-details">
			<h1>Nouvelle offre d'emploi</h1>
			<p className="pxp-text-light">
				Ajoutez un nouveau poste à la liste des postes de votre
				entreprise.
			</p>
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="row mt-4 mt-lg-5">
					<div className="row">
						<TextInput
							name="name"
							label="Titre d'emploi"
							register={register}
							errors={errors}
						/>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<Select
								name="cityId"
								label="Ville"
								options={city}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<Select
								name="countryId"
								label="Pays"
								options={country}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<div className="row">
						<Select
							name="domaineDactiviteId"
							label="Domaine d'activité"
							options={domaines}
							register={register}
							errors={errors}
						/>
					</div>
					<div className="row">
						<Textarea
							name={"discription"}
							label={"Description de l'emploi"}
							register={register}
							errors={errors}
						/>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<Select
								name={"yearsExp"}
								label={"Expérience"}
								options={years}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<Select
								name={"carrerLevelId"}
								label={"Niveau de carrière"}
								options={carrerLevel}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<Select
								name={"employmentTypeId"}
								label={"Type d'emploi"}
								options={employmentType}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<Select
								name={"salaryRangeId"}
								label={"Échelle salariale"}
								options={salaryRange}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
				</div>
				<div className="mt-4 mt-lg-5">
					<button
						type="submit"
						className="btn rounded-pill pxp-section-cta"
					>
						Enregistrer
					</button>
				</div>
			</form>
		</div>
	);
};

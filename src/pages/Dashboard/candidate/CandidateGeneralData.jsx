import React from "react";
import {
	TextInput,
	BooleanInput,
	RadioInput,
} from "../../../components/inputDash/index";
import { instance } from "../../../lib/axios";
import { Eggy } from "@s-r0/eggy-js";
import { useForm } from "react-hook-form";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";
import { v4 as uuidv4 } from "uuid";

const languageLevel = ["very_good", "good", "average", "weak"];
const languageLevelRadioGroup = [
	{ id: uuidv4(), label: "Très bien", value: "very_good" },
	{ id: uuidv4(), label: "Bien", value: "good" },
	{ id: uuidv4(), label: "Moyen", value: "average" },
	{ id: uuidv4(), label: "Faible", value: "weak" },
];

// function formatDate(date) {
// 	var d = new Date(date),
// 		month = "" + (d.getMonth() + 1),
// 		day = "" + d.getDate(),
// 		year = d.getFullYear();

// 	if (month.length < 2) month = "0" + month;
// 	if (day.length < 2) day = "0" + day;

// 	return [year, month, day].join("-");
// }

export default function CandidateGeneralData({ data }) {
	const { profile, id } = data;
	const [generalData, setGeneralData] = React.useState({});

	const schema = Joi.object({
		// 3
		firstName: Joi.string().allow(null, ""),
		lastName: Joi.string().allow(null, ""),
		phone: Joi.string().allow(null, ""),
		email: Joi.string().allow(null, ""),
		//

		birthDate: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		city: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		maritalStatus: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),

		// secoend part => 4
		educationLevel: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		studyField: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		graduationYear: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		diplome: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),

		// third part => 10
		havingFrenchTest: Joi.boolean().allow(null, "").messages({
			"boolean.base": "{{#label}} must be a boolean",
		}),
		frenchTestLabel: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		frenchTestScore: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		frenchTestYear: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		frenchLevel: Joi.any()
			.valid(...languageLevel)
			.messages({
				"any.only":
					"{{#label}} failed custom validation because {{#error.message}}",
			}),

		havingEnglishTest: Joi.boolean().allow(null, "").messages({
			"boolean.base": "{{#label}} must be a boolean",
		}),
		englishTestLabel: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		englishTestScore: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		englishTestYear: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		englishLevel: Joi.any()
			.valid(...languageLevel)
			.messages({
				"any.only":
					"{{#label}} failed custom validation because {{#error.message}}",
			}),

		// last part => 12
		havingJob: Joi.boolean().allow(null, "").messages({
			"boolean.base": "{{#label}} must be a boolean",
		}),
		jobField: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		employerName: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		experienceYears: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),

		havingCNSS: Joi.boolean().allow(null, "").messages({
			"boolean.base": "{{#label}} must be a boolean",
		}),
		declaredDays: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),

		havingOtherJob: Joi.boolean().allow(null, "").messages({
			"boolean.base": "{{#label}} must be a boolean",
		}),
		otherJobField: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		otherJobEmployerName: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		otherJobExperienceYears: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),

		havingCNSSOtherJob: Joi.boolean().allow(null, "").messages({
			"boolean.base": "{{#label}} must be a boolean",
		}),
		otherJobDeclaredDays: Joi.string().allow(null, "").messages({
			"string.empty": "Ce champ est obligatoire",
		}),
	});

	const {
		watch,
		control,
		register,
		unregister,
		handleSubmit,
		formState: { errors, isDirty, isSubmitting },
		reset,
	} = useForm({
		resolver: joiResolver(schema),
	});

	const onSubmit = (data) => {
		console.log("OnSubmit ??", data);

		instance
			.put(`/ws101form/${id}`, data)
			.then(async (res) => {
				await Eggy({
					title: "Success",
					message: "profile updated !",
					type: "success",
				});
			})
			.catch(async (err) => {
				await Eggy({
					title: "Error",
					message: "profile doesn't updated !",
					type: "error",
				});
				console.log(err);
			});
	};

	React.useEffect(() => {
		instance
			.get(`/ws101form/${id}`)
			.then((res) => {
				const data = res?.data?.results;
				console.log("this is General Data", data);
				setGeneralData(data);
				// const birthDate = data?.birthDate
				// 	? formatDate(data?.birthDate)
				// 	: "";

				// reset([
				// 	{ birthDate: birthDate },
				// 	{ havingFrenchTest: data?.havingFrenchTest },
				// 	{ havingEnglishTest: data?.havingEnglishTest },
				// 	{ havingJob: data?.havingJob },
				// 	{ havingCNSS: data?.havingCNSS },
				// 	{ havingOtherJob: data?.havingOtherJob },
				// 	{ havingCNSSOtherJob: data?.havingCNSSOtherJob },
				// ]);
			})
			.catch((err) => {
				console.log(err);
			});
	}, [id, reset]);

	return (
		<div className="pxp-dashboard-content-details">
			<h1>
				Questionnaire de collecte et d'analyse des informations de
				l'employé
			</h1>
			{/* <p>{JSON.stringify(errors ? errors : {})}</p> */}
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="row mt-4 mt-lg-5">
					<h2>Information sur le client :</h2>
					<div className="row">
						<div className="col-sm-6">
							<TextInput
								name="firstName"
								label="Nom"
								defaultValue={profile?.firstName}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<TextInput
								name="lastName"
								label="Prénom"
								defaultValue={profile?.lastName}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<TextInput
								name="birthDate"
								label="Date de naissance"
								type="date"
								defaultValue={generalData?.birthDate}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<TextInput
								name="phone"
								label="Numéro de téléphone"
								defaultValue={profile?.phone}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<TextInput
								name="email"
								label="Adresse mail"
								defaultValue={profile?.email}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<TextInput
								name="city"
								label="Ville"
								defaultValue={generalData?.city}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-12">
							<TextInput
								name="maritalStatus"
								label="Situation matrimoniale"
								defaultValue={generalData?.maritalStatus}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
				</div>

				{/* part 2 */}
				<div className="row mt-4 mt-lg-5">
					<h2>Information sur son niveau d'étude :</h2>
					<div className="row">
						<div className="col-sm-6">
							<TextInput
								name="educationLevel"
								label="Niveau d'étude"
								defaultValue={generalData?.educationLevel}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<TextInput
								name="studyField"
								label="Domaine d'étude"
								defaultValue={generalData?.studyField}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<TextInput
								name="diplome"
								label="Diplôme"
								defaultValue={generalData?.diplome}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<TextInput
								name="graduationYear"
								label="Année d'obtention"
								defaultValue={generalData?.graduationYear}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
				</div>

				{/* part 3 */}
				<div className="row mt-4 mt-lg-5">
					<h2>Information sur son niveau de langue :</h2>
					<div className="row">
						<div className="mb-3">
							<label className="form-label">
								Avez-vous déjà effectué un test de français ?
							</label>
							<br />
							<BooleanInput
								control={control}
								name={"havingFrenchTest"}
								unregisterTruthy={() =>
									unregister("frenchLevel")
								}
								unregisterFlasy={() =>
									[
										"frenchTestLabel",
										"frenchTestScore",
										"frenchTestYear",
									].forEach((el) => unregister(el))
								}
							/>
						</div>
					</div>

					{watch()?.havingFrenchTest && (
						<>
							<div className="row">
								<div className="col-sm-12">
									<TextInput
										name="frenchTestLabel"
										label="Si oui, lequel :"
										defaultValue={
											generalData?.frenchTestLabel
										}
										register={register}
										errors={errors}
									/>
								</div>
							</div>

							<div className="row">
								<div className="col-sm-6">
									<TextInput
										name="frenchTestScore"
										label="Score obtenu :"
										defaultValue={
											generalData?.frenchTestScore
										}
										register={register}
										errors={errors}
									/>
								</div>

								<div className="col-sm-6">
									<TextInput
										name="frenchTestYear"
										label="Année du test :"
										defaultValue={
											generalData?.frenchTestYear
										}
										register={register}
										errors={errors}
									/>
								</div>
							</div>
						</>
					)}

					{watch()?.havingFrenchTest === false && (
						<div className="row">
							<div className="mb-3">
								<label className="form-label">
									Si non, niveau :
								</label>
								<br />
								<RadioInput
									control={control}
									name={"frenchLevel"}
									radioGroup={languageLevelRadioGroup}
								/>
							</div>
						</div>
					)}

					<div className="row">
						<div className="mb-3">
							<label className="form-label">
								Avez-vous déjà effectué un test d'anglais ?
							</label>
							<br />
							<BooleanInput
								control={control}
								name={"havingEnglishTest"}
								unregisterTruthy={() =>
									unregister("englishLevel")
								}
								unregisterFlasy={() =>
									[
										"englishTestLabel",
										"englishTestScore",
										"englishTestYear",
									].forEach((el) => unregister(el))
								}
							/>
						</div>
					</div>

					{watch()?.havingEnglishTest && (
						<>
							<div className="row">
								<div className="col-sm-12">
									<TextInput
										name="englishTestLabel"
										label="Si oui, lequel :"
										defaultValue={
											generalData?.englishTestLabel
										}
										register={register}
										errors={errors}
									/>
								</div>
							</div>

							<div className="row">
								<div className="col-sm-6">
									<TextInput
										name="englishTestScore"
										label="Score obtenu :"
										defaultValue={
											generalData?.englishTestScore
										}
										register={register}
										errors={errors}
									/>
								</div>

								<div className="col-sm-6">
									<TextInput
										name="englishTestYear"
										label="Année du test :"
										defaultValue={
											generalData?.englishTestYear
										}
										register={register}
										errors={errors}
									/>
								</div>
							</div>
						</>
					)}

					{watch()?.havingEnglishTest === false && (
						<div className="row">
							<div className="mb-3">
								<label className="form-label">
									Si non, niveau :
								</label>
								<br />
								<RadioInput
									control={control}
									name={"englishLevel"}
									radioGroup={languageLevelRadioGroup}
								/>
							</div>
						</div>
					)}
				</div>

				{/* part 4 */}
				<div className="row mt-4 mt-lg-5">
					<h2>Information sur son expérience professionnelle :</h2>
					<div className="row">
						<div className="mb-3">
							<label className="form-label">
								Êtes-vous en activité ?
							</label>
							<br />
							<BooleanInput
								control={control}
								name={"havingJob"}
								unregisterFlasy={() =>
									[
										"jobField",
										"employerName",
										"experienceYears",
										"havingCNSS",
										"declaredDays",
										"havingOtherJob",
										"otherJobField",
										"otherJobEmployerName",
										"otherJobExperienceYears",
										"havingCNSSOtherJob",
										"otherJobDeclaredDays",
									].forEach((el) => unregister(el))
								}
							/>
						</div>
					</div>

					{watch()?.havingJob && (
						<>
							<div className="row">
								<div className="col-sm-12">
									<TextInput
										name="jobField"
										label="Si oui, en quel domaine :"
										alt="Domaine d'activité"
										defaultValue={generalData?.jobField}
										register={register}
										errors={errors}
									/>
								</div>
							</div>

							<div className="row">
								<div className="col-sm-6">
									<TextInput
										name="employerName"
										label="Employeur :"
										alt="Employeur"
										defaultValue={generalData?.employerName}
										register={register}
										errors={errors}
									/>
								</div>

								<div className="col-sm-6">
									<TextInput
										name="experienceYears"
										label="Année (s) d'expérience :"
										alt="Année (s) d'expérience"
										defaultValue={
											generalData?.experienceYears
										}
										register={register}
										errors={errors}
									/>
								</div>
							</div>

							{/* ✅ */}
							<div className="row">
								<div className="mb-3">
									<label className="form-label">
										Êtes-vous déclaré en CNSS ?
									</label>
									<br />
									<BooleanInput
										control={control}
										name={"havingCNSS"}
										unregisterFlasy={() =>
											unregister("declaredDays")
										}
									/>
								</div>
							</div>

							{watch()?.havingCNSS && (
								<div className="row">
									<div className="col-sm-12">
										<TextInput
											name="declaredDays"
											label="Si oui, combien de jours déclarés"
											alt={"Jours déclarés"}
											defaultValue={
												generalData?.declaredDays
											}
											register={register}
											errors={errors}
										/>
									</div>
								</div>
							)}

							<div className="row">
								<div className="mb-3">
									<label className="form-label">
										Exerciez-vous avant un autre emploi ?
									</label>
									<br />
									<BooleanInput
										control={control}
										name={"havingOtherJob"}
										unregisterFlasy={() =>
											[
												"otherJobField",
												"otherJobEmployerName",
												"otherJobExperienceYears",
												"havingCNSSOtherJob",
												"otherJobDeclaredDays",
											].forEach((el) => unregister(el))
										}
									/>
								</div>
							</div>
						</>
					)}

					{watch()?.havingOtherJob && (
						<>
							<div className="row">
								<div className="col-sm-12">
									<TextInput
										name="otherJobField"
										label="Si oui, en quel domaine :"
										alt="Domaine d'activité"
										defaultValue={
											generalData?.otherJobField
										}
										register={register}
										errors={errors}
									/>
								</div>
							</div>

							{/* ✅ */}
							<div className="row">
								<div className="col-sm-6">
									<TextInput
										name="otherJobEmployerName"
										label="Employeur :"
										alt="Employeur"
										defaultValue={
											generalData?.otherJobEmployerName
										}
										register={register}
										errors={errors}
									/>
								</div>

								<div className="col-sm-6">
									<TextInput
										name="otherJobExperienceYears"
										label="Année (s) d'expérience :"
										alt="Année (s) d'expérience"
										defaultValue={
											generalData?.otherJobExperienceYears
										}
										register={register}
										errors={errors}
									/>
								</div>
							</div>

							<div className="row">
								<div className="mb-3">
									<label className="form-label">
										Êtes-vous déclaré en CNSS ?
									</label>
									<br />
									<BooleanInput
										control={control}
										name={"havingCNSSOtherJob"}
										unregisterFlasy={() =>
											unregister("otherJobDeclaredDays")
										}
									/>
								</div>
							</div>

							{watch()?.havingCNSSOtherJob && (
								<div className="row">
									<div className="col-sm-12">
										<TextInput
											name="otherJobDeclaredDays"
											label="Si oui, combien de jours déclarés"
											alt={"Jours déclarés"}
											defaultValue={
												generalData?.declaredDays
											}
											register={register}
											errors={errors}
										/>
									</div>
								</div>
							)}
						</>
					)}
				</div>

				<div className="mt-4 mt-lg-5">
					<button
						// type="submit"
						className="btn rounded-pill pxp-section-cta"
						disabled={!isDirty || isSubmitting}
					>
						Enregistrer
					</button>
				</div>
			</form>
		</div>
	);
}

import React, { useState, useEffect } from "react";
import { instance } from "../../../lib/axios";
import { CandidatesbodyTableList } from "../../../components/table/CandidatesbodyTableList";
import Pagination from "../../../components/pagination/Pagination";
export default function CadidatesListe() {
	const [condidates, setCondidates] = useState([]);
	const [loading, setLoading] = useState(true);
	const [totale,setTotale] = useState(0);
	const [allcandidates, setAllCandidtes]= useState({});
	const [currentPage, setCurrentPage] = useState(1);
	const Search = (e) => {
		const filteredCandidates = allcandidates.filter(({ phone, firstName,lastName,email }) =>
		phone.toLowerCase().includes(e.toLowerCase()) || firstName.toLowerCase().includes(e.toLowerCase())
		|| lastName.toLowerCase().includes(e.toLowerCase()) 
		|| email.toLowerCase().includes(e.toLowerCase()) 
		);
		setCondidates(filteredCandidates);
   };

	  function isFloat(value) {
		if (
		  typeof value === 'number' &&
		  !Number.isNaN(value) &&
		  !Number.isInteger(value)
		) {
		  return true;
		}
	  
		return false;
	  }
    useEffect(()=>{
		const skip = 0;
        const take = 15;
        instance.get(`/candidates?skip=${skip}&take=${take}`).then((response)=>{
			setCondidates(response.data.results.allcondidates.precadidates) 
			setTotale(response.data.results.candidatesCount)
			setLoading(false)  
		}) 
    }, [])
	useEffect(()=>{
        const skip = (currentPage - 1) * 15;
        const take = 15;
        instance.get(`/candidates?skip=${skip}&take=${take}`).then((response)=>{
			setCondidates(response.data.results.allcondidates.precadidates) 
			setTotale(response.data.results.candidatesCount)
			setLoading(false) 
		})  
    }, [currentPage])
	const handlePageChange = (newPage) => {
		setCurrentPage(newPage);
	  };
return (
		<>
		{
			loading? (
						<div className="pxp-dashboard-content-details">
								<div className="text-center">
									<div className="spinner-border" role="status">
										<span className="sr-only">Loading...</span>
									</div>
								</div>
						</div>		
						) : 
						(
							totale ===0 ?
							<div className="pxp-dashboard-content-details">
								<div style={{textAlign:'center'}}>
									<span>Aucun candidature  Pour le moment</span>
								</div>
							</div>
						:
							<div className="pxp-dashboard-content-details">
								<div className="mt-4 mt-lg-5">
									<div className="row justify-content-between align-content-center">
										<div className="col-auto order-2 order-sm-1">
											<div className="pxp-candidate-dashboard-jobs-bulk-actions mb-3">
												<h1>Candidatures</h1>                               
											</div>
										</div>
										<div className="col-auto order-1 order-sm-2">
											<div className="pxp-candidate-dashboard-jobs-search mb-3">
												<div className="pxp-candidate-dashboard-jobs-search-results me-3">{totale} Candidates</div>
												<div className="pxp-candidate-dashboard-jobs-search-search-form">
													<div className="input-group">
														<span className="input-group-text"><span className="fa fa-search"></span></span>
														<input type="text" className="form-control" placeholder="Recherche de candidat..." onChange={({ target }) => Search(target.value)}/>
													</div>
												</div>
											</div>
										</div>    
									</div> 
								</div>    
								<div className="table-responsive">
									<table className="table table-hover align-middle">
										<thead>
												<tr>
													<th>Nom</th>
													<th>Téléphone</th>
													<th >Offre</th>
													<th >Entreprise	</th>
													<th>&nbsp;</th>
												</tr>
											</thead>
										<CandidatesbodyTableList data={condidates} />
									</table>
									<Pagination currentPage={currentPage} totalPages={isFloat(totale/15) ? Math.floor(totale/15) +1  : (totale/15)} onPageChange={handlePageChange}/>
							</div>
							</div>
						)
		}
			
	
		</>
	);
}

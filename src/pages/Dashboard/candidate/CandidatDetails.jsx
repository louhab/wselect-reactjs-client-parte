import React, { useEffect, useState } from "react";
import {instance} from '../../../lib/axios'
export default function CandidatDetails({id, setComponent}) {
	const [candidate, setCandidate] = useState([])
	useEffect(() => {
		instance
			.get(`/candidates/${id}`)
			.then((res) => {
				setCandidate(res.data.results)
			})
			.catch((err) => {
				console.log(err);
			});
	}, []);
	return (
		<>
			<div  >
			<h1 className="mx-auto">Candidature </h1>
				<div className="row">
					<div className="col-9">
						<div className="mt-3 mt-lg-4">
							<div className="pxp-single-job-side-company">
								<button className="btn rounded-pill pxp-section-cta d-block" title="retour" onClick={()=>setComponent('Dashboard')}>
									<span className="fa fa-arrow-left"></span>
								</button>
							</div>
							<div className="mt-4">
								<div className="pxp-single-job-side-info-label pxp-text-light">Nom et prénom  du candidat</div>
								<div className="pxp-single-job-side-info-data">{candidate.firstName} {candidate.lastName} </div>
							</div>
							
							<div className="mt-4">
								<div className="pxp-single-job-side-info-label pxp-text-light">Email du candidat </div>
								<div className="pxp-single-job-side-info-data">{candidate.email}</div>
							</div>
							<div className="mt-4">
								<div className="pxp-single-job-side-info-label pxp-text-light">Téléphone du candidat</div>
								<div className="pxp-single-job-side-info-data">{candidate.phone}</div>
							</div>
							
							<div className="mt-4">
								<div className="pxp-single-job-side-info-label pxp-text-light">Ville du candidat</div>
								<div className="pxp-single-job-side-info-data">Casablanca</div>
							</div>

							<div className="mt-4">
								<div className="pxp-single-job-side-info-label pxp-text-light">Domaine d'activite</div>
								<div className="pxp-single-job-side-info-data">{candidate.diplomasName}</div>
							</div>

							<div className="mt-4">
								<div className="pxp-single-job-side-info-label pxp-text-light">Diplome du candidat </div>
								<div className="pxp-single-job-side-info-data">{candidate.diplomasName}</div>
							</div>
							<div className="mt-4">
								<div className="pxp-single-job-side-info-label pxp-text-light">CV du candidat </div>
								<div className="pxp-single-job-side-info-data">  
									<a  href={`${process.env.REACT_APP_API_STATIC}/static/${candidate.cvPath}`}  title="Télécharger"><span className="fa fa-download"></span>Télécharger</a>
								</div>
							</div>
							
						</div>
					</div>	
				</div>	
			</div>
		</>
	);
}

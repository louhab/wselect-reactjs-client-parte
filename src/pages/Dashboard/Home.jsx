/* eslint-disable no-unused-vars */
import React, { useEffect, useState, useContext } from "react";
import { AuthContext } from "../../context/Auth";
import EmployerDetails from "./empoyerFile";
import CondidateDetails from "./candidatefile";

const Home = () => {
	const { auth } = useContext(AuthContext);
	useEffect(() => {}, [auth]);
	return (
		<div
			style={{
				backgroundColor: "#FFF8EC",
				display: "flex",
				flexDirection: "row",
				height: "100%",
			}}
		>
			{auth?.type === "c" ? (
				<CondidateDetails id={auth?.id} />
			) : auth?.type === "e" ? (
				<EmployerDetails id={auth?.id} />
			) : (
				<>loading...</>
			)}
		</div>
	);
};

export default Home;

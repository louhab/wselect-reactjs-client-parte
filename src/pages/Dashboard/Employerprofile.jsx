import React from "react";
import { TextInput } from "../../components/inputDash/index";
import { instance } from "../../lib/axios";
import { Eggy } from "@s-r0/eggy-js";
import { useForm } from "react-hook-form";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";

const Employerprofile = ({ data }) => {
	const { profile, id } = data;

	const schema = Joi.object({
		contactName: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		contactPhone: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		email: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		phone: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
		address: Joi.string().messages({
			"string.empty": "Ce champ est obligatoire",
		}),
	});

	const {
		register,
		handleSubmit,
		formState: { errors },
	} = useForm({
		resolver: joiResolver(schema),
	});

	const onSubmit = (values) => {
		instance
			.put(`/employers/${id}`, {
				...values,
			})
			.then(async (res) => {
				await Eggy({
					title: "Success",
					message: "profile updated !",
					type: "success",
				});
			})
			.catch(async (err) => {
				await Eggy({
					title: "Error",
					message: "profile doesn't update !",
					type: "error",
				});
				console.log(err);
			});
	};

	return (
		<div className="pxp-dashboard-content-details">
			<h1>Editer le profil</h1>
			<p className="pxp-text-light">
				Modifiez les informations de votre page de profil d'employer.
			</p>
			<form onSubmit={handleSubmit(onSubmit)}>
				<div className="row mt-4 mt-lg-5">
					<div className="row">
						<div className="col-sm-6">
							<TextInput
								name="contactName"
								label="Nom"
								defaultValue={profile?.contactName}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<TextInput
								name="email"
								label="Email"
								defaultValue={profile?.email}
								register={register}
								errors={errors}
							/>
						</div>
						<div className="col-sm-6">
							<TextInput
								name="contactPhone"
								label="Phone"
								defaultValue={profile?.contactPhone}
								register={register}
								errors={errors}
							/>
						</div>
					</div>
					<TextInput
						name="address"
						label="Address"
						defaultValue={profile?.address}
						register={register}
						errors={errors}
					/>
				</div>
				<div className="mt-4 mt-lg-5">
					<button
						type="submit"
						className="btn rounded-pill pxp-section-cta"
					>
						Enregistrer
					</button>
				</div>
			</form>
		</div>
	);
};

export default Employerprofile;

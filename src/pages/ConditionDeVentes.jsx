import React from "react";
import Footer from "../components/layout/Footer";
import Header from "../components/layout/Header";
import ConditiondeVentes from "../components/conditions/ConditiondeVentes";
// import Logo from "../assets/images/Logo-HR-Globe-Profil.png";
import Logo from "../assets/images/logo-ws.png";

import AboutHero from "../components/about/AboutHero";
import Background from "../assets/images/Condition-de-vente.jpg";

export default function ConditionDeVentes() {
	return (
		<>
			<Header
				logo={Logo}
				header="pxp-header fixed-top pxp-bg pxp-is-sticky"
					nav="pxp-nav dropdown-hover-all d-none d-xl-block"
					extraNav="pxp-user-nav d-none d-sm-flex"
			/>
						<AboutHero
				bg={Background}
				content={() => (
					<div className="text-center">
						<h1
							style={{
								fontWeight: 50,
								fontSize: "48px",
							}}
						>
							<b
								style={{
									fontWeight: "bold",
									fontSize: "48px",
								}}
							>
								Conditions De Ventes{" "}
							</b>
						</h1>
					</div>
				)}
			/>
			<ConditiondeVentes />
			<Footer />
		</>
	);
}

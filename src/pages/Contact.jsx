import React, { Suspense } from "react";
// import Logo from "../assets/images/Logo-HR-Globe-Profil.png";
import Logo from "../assets/images/logo-ws.png";
import PreLoader from "../components/PreLoader";

const Header = React.lazy(() => import("../components/layout/Header"));
const Footer = React.lazy(() => import("../components/layout/Footer"));
const ContactComponent = React.lazy(() => import("../components/Contact"));

export default function Contact() {
	return (
		<>
			<Suspense fallback={<PreLoader />}>
				<Header
					logo={Logo}
					header="pxp-header fixed-top pxp-bg pxp-is-sticky"
					nav="pxp-nav dropdown-hover-all d-none d-xl-block"
					extraNav="pxp-user-nav d-none d-sm-flex"
				/>
				<ContactComponent />
				<Footer />
			</Suspense>
		</>
	);
}

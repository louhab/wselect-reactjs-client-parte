import React, { Suspense } from "react";
import Logo from "../assets/images/logo-ws.png";
import PreLoader from "../components/PreLoader";

const MinimalHeader = React.lazy(() =>
	import("../components/layout/MinimalHeader")
);
const EmployerLoginForm = React.lazy(() =>
	import("../components/forms/EmployerLoginForm")
);

export default function EmployerSignIn() {
	return (
		<>
			<Suspense fallback={<PreLoader />}>
				<MinimalHeader
					logo={Logo}
					header="pxp-header fixed-top pxp-no-bg pxp-has-border"
					nav="pxp-nav dropdown-hover-all d-none d-xl-block"
					extraNav="pxp-user-nav d-none d-sm-flex pxp-on-light"
				/>
				<EmployerLoginForm />
			</Suspense>
		</>
	);
}

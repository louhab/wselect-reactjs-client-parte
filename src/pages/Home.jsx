import React from "react";
import Header from "../components/Header";

export default function Home() {
	return <Header 
		header="pxp-header fixed-top pxp-no-bg"
		nav="pxp-nav dropdown-hover-all pxp-light  d-none d-xl-block"
		extraNav="pxp-user-nav d-none d-sm-flex"
	/>;
}
